@extends('layouts.masterauth')

@section('content')
<div class="logo">
    <span class="db"><img src="{{ asset('adminbite/assets/images/study.svg') }}" width="30px" alt="logo" /></span>
    <h5 class="font-medium m-b-20">Register</h5>
</div>
<!-- Form -->
<div class="row">
    <div class="col-12">
        <form class="form-horizontal m-t-20" id="loginform" action="{{ route('register') }}" method="POST">
            @csrf
            <div class="form-group">
                <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" required placeholder="Fullname" autocomplete="name">
                @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <input type="email" name="email" id="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" required placeholder="email@example.com" autocomplete="email">
                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <input type="password" name="password" id="password" class="form-control @error('password') is-invalid @enderror" value="{{ old('password') }}" required placeholder="Password" autocomplete="new-password">
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <input type="password" name="password_confirmation" id="password-confirm" class="form-control @error('password-confirm') is-invalid @enderror" value="{{ old('password-confirm') }}" required placeholder="Confirm Password" autocomplete="new-password">
                @error('password-confirm')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group text-center">
                <div class="col-xs-12 p-b-20">
                    <button type="submit" class="btn btn-block btn-lg btn-info" type="submit">Register</button>
                </div>
            </div>
            <div class="form-group m-b-0 m-t-10">
                <div class="col-sm-12 text-center">
                    Already have an account? <a href="{{ route('login') }}" class="text-info m-l-5"><b>Sign
                            In</b></a>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection