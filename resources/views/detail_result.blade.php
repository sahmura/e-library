@extends('layouts.app')

@section('content')
<div class="fix-width" style="min-height: 47vh;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3>{{ $material->title }}</h3>
                <hr>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-md-12 mb-3">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <img class="img-responsive img-fluid" src="{{ url('cover/' . $material->cover_path) }}" alt="{{ $material->title }}">
                            </div>
                            <div class="col-md-6">
                                <table class="table table-borderles">
                                    <tr>
                                        <td>Judul</td>
                                        <td>{{ $material->title }}</td>
                                    </tr>
                                    <tr>
                                        <td>ISBN</td>
                                        <td>{{ $material->isbn }}</td>
                                    </tr>
                                    <tr>
                                        <td>Edisi</td>
                                        <td>{{ $material->edition }}</td>
                                    </tr>
                                    <!-- <tr>
                                <td>Volume</td>
                                <td>{{ $material->volume }}</td>
                            </tr>
                            <tr>
                                <td>Number</td>
                                <td>{{ $material->number }}</td>
                            </tr> -->
                                    <tr>
                                        <td>Publisher</td>
                                        <td>{{ $material->publisher }}</td>
                                    </tr>
                                    <tr>
                                        <td>Tahun Rilis</td>
                                        <td>{{ \Carbon\Carbon::parse($material->year)->locale('id')->isoFormat('MMMM YYYY') }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Kategori</td>
                                        <td>{{ $material->category->name }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Keywords</td>
                                        <td>{{ $material->keywords}}
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 mb-3">
                <div class="card">
                    <div class="card-body">
                        <div class="row d-flex align-items-center">
                            <div class="col-md-2">
                                Dibaca<br>
                                {{ $material->reads }}
                            </div>
                            <div class="col-md-2">
                                Diunduh <br>
                                {{ $material->downloads }} kali
                            </div>
                            <div class="col-md-8 text-right">
                                <a href="{{ route('login') }}" class="btn waves-effect waves-light btn-primary">Login
                                    untuk pinjam atau
                                    unduh</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 mb-3">
                <div class="card">
                    <div class="card-body">
                        @if($material->abstract != '')
                        <h5>Abstrak</h5>
                        {{ $material->abstract }}
                        @endif
                        @if($material->description != '')
                        <h5 class="mt-3">Deskripsi</h5>
                        {{ $material->description }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('js')
<script>
    $('#btn-filter').on('click', function() {
        var type = $('#type_id').val();
        var category = $('#category_id').val();
        var tahun = $('#tahun').val();
        var key = encodeURI($('#key').val());
        window.location.href = "{{ url('search/') }}" + '/' + type + '/' + category + '/' +
            tahun + '/' + key;
    })
</script>
@endpush