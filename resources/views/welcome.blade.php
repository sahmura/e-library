@extends('layouts.app')

@section('content')
<div class="fix-width">
    <div class="row banner-text justify-content-center">
        <div class="col-md-8 m-t-20 text-center">
            <img src="{{ url('adminbite/assets/images/unnes.png') }}" alt="Logo Unnes" width="100px" class="img-fluid mb-3">
            <h3>PASCASARJANA UNIVERSITAS NEGERI SEMARANG</h3>
            <h1 class="mt-5">Digital Library
                <!-- <span class="text-danger">Bootstrap 4</span> Admin Template</h1> -->
                <p class="subtext">
                    <span class="font-medium">Sumber Literasi </span>Dunia
                    <div class="down-btn">
                        <a href="{{ url('search') }}" class="btn btn-info m-b-10">Jelajah Literasi</a>
                        <a href="{{ url('statistik') }}" class="btn btn-danger m-b-10">Statistik</a>
                    </div>
                </p>
            </h1>
        </div>
        <div class="col-md-12 text-center">
            <div class="hero-banner">
                <img src="{{ asset('adminbite/assets/images/coverdepan.svg') }}" alt="Cover Depan" class="img-fluid" />
            </div>
        </div>
    </div>
</div>
@endsection