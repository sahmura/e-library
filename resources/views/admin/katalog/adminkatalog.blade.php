@extends('layouts.master')

@push('css')
<link href="{{ asset('adminbite/assets/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}"
    rel="stylesheet">
@endpush

@section('bc')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Katalog</h4>
            <div class="d-flex align-items-center">

            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{ url('admin') }}">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Katalog</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <a href="{{ url('admin/katalog/create') }}" class="btn btn-info float-right"
                    id="btn-riwayat-peminjam"><i class="mdi mdi-plus"></i> Tambah
                    Data</a>
                <div class="table-responsive mt-5">
                    <table class="table table-striped" id="dataTable">
                        <thead>
                            <th style="width: 20px;text-align: center;">No</th>
                            <th style="width: 130px;">ISBN / Series</th>
                            <th>Judul</th>
                            <th style="width: 100px;">Kategori</th>
                            <th style="width: 50px; text-align:center;"><i class="mdi mdi-arrange-bring-forward"></i>
                            </th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script src="{{ asset('adminbite/assets/extra-libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script>
    function refresh_data() {
        $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            destroy: true,
            ajax: {
                url: "{{ url('admin/katalog') }}",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'GET',
            },
            columns: [{
                    data: 'DT_RowIndex',
                    searchable: false,
                    class: 'text-center',
                    width: '20px'
                },
                {
                    data: 'isbn'
                },
                {
                    data: 'title'
                },
                {
                    data: 'category'
                },
                {
                    data: 'action',
                    searchable: false,
                    class: 'text-center',
                },
            ]

        });
    }

    $(document).ready(function () {
        refresh_data();
    });

    $(document).on('click', '.btn-delete', function () {
        var id = $(this).data('id');
        Swal.fire({
            title: 'Apakah Anda Yakin?',
            text: 'Semua data termasuk peminjaman akan dihapus',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Hapus",
        }).then((Confirm) => {
            if (Confirm.value) {
                $.ajax({
                    url: "{{ url('admin/katalog/deleteData') }}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: 'POST',
                    data: {
                        id: id
                    },
                    success: function (data) {
                        Swal.fire({
                            title: data.title,
                            text: data.text,
                            icon: data.icon,
                        }).then((Confirm) => {
                            refresh_data();
                        })
                    }
                })
            }
        })
    });

    $(document).on('click', '.btn-edit', function () {
        var id = $(this).data('id');
        window.location.href = "{{ url('admin/katalog/') }}" + '/' + id + '/edit';
    });

    $(document).on('click', '.btn-detail', function () {
        var id = $(this).data('id');
        window.location.href = "{{ url('admin/katalog/') }}" + '/' + id + '/detail';
    });

</script>
@if(Session::get('icon'))
<script>
    Swal.fire({
        title: "{{ Session::get('title') }}",
        text: "{{ Session::get('text') }}",
        icon: "{{ Session::get('icon') }}",
    }).then((Confirm) => {
        window.location.reload();
    })

</script>
@endif
@endpush
