@extends('layouts.master')

@push('css')
<link href="{{ asset('adminbite/assets/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">
<style>
    @media (max-width: 991.98px) {
        .text-center-mobile {
            text-align: center;
        }

        .mt-mobile {
            margin-top: 2em;
        }
    }
</style>
@endpush

@section('bc')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Katalog</h4>
            <div class="d-flex align-items-center">

            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{ url('admin') }}">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="{{ url('admin/katalog') }}">Katalog</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Edit</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <a href="{{ url('admin/katalog/' . $data->id . '/edit') }}" class="btn btn-warning float-right" id="btn-riwayat-peminjam"><i class="mdi mdi-pencil"></i> Sunting
            Data</a>
    </div>
</div>
<div class="card mt-3">
    <div class="card-body">
        <div class="row d-flex align-items-start">
            <div class="col-md-3 text-center-mobile">
                <img src="{{ url('cover/' . $data->cover_path) }}" alt="Cover" style="height: 300px;">
            </div>
            <div class="col-md-6 mt-mobile">
                <h3>{{ $data->title }}</h3>
                <hr>
                <p>
                    @foreach($data->authors as $author)
                    {{ $author->name }}
                    @if(!$loop->last)
                    ,
                    @else
                    .
                    @endif
                    @endforeach
                </p>
                @if($data->abstract != '')
                <h5 class="mt-4">Abstrak</h5>
                <p>{{ $data->abstract }}</p>
                @endif
                @if($data->description != '')
                <h5 class="mt-4">Deskripsi</h5>
                <p>{{ $data->description }}</p>
                @endif
                @if($data->isbn != '')
                <h5 class="mt-4">ISBN</h5>
                <p>{{ $data->isbn }}</p>
                @endif
                @if($data->edition != '')
                <h5 class="mt-4">Edisi</h5>
                <p>{{ $data->edition }}</p>
                @endif
                @if($data->volume != '')
                <h5 class="mt-4">Volume</h5>
                <p>{{ $data->volume }}</p>
                @endif
            </div>
            <div class="col-md-3 text-center">
                <a href="{{ url('file/' . $data->download_path) }}" class="btn btn-block btn-outline-primary" target="_blank"><i class="mdi mdi-download"></i> Download</a>
                <button class="btn btn-success btn-block mt-2">Diunduh {{ $data->downloads }}
                    kali</button>
                <button class="btn btn-warning btn-block mt-2">Dibaca {{ $data->reads }} kali</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script src="{{ asset('adminbite/assets/extra-libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script>
    function refresh_data() {
        $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            destroy: true,
            ajax: {
                url: "{{ url('admin/katalog') }}",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'GET',
            },
            columns: [{
                    data: 'DT_RowIndex',
                    searchable: false,
                    class: 'text-center',
                    width: '20px'
                },
                {
                    data: 'isbn'
                },
                {
                    data: 'title'
                },
                {
                    data: 'category'
                },
                {
                    data: 'action',
                    searchable: false,
                    class: 'text-center',
                },
            ]

        });
    }

    $(document).ready(function() {
        refresh_data();
    });

    $(document).on('click', '.btn-delete', function() {
        var id = $(this).data('id');
        Swal.fire({
            title: 'Apakah Anda Yakin?',
            text: 'Semua data termasuk peminjaman akan dihapus',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Hapus",
        }).then((Confirm) => {
            if (Confirm.value) {
                $.ajax({
                    url: "{{ url('admin/katalog/deleteData') }}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: 'POST',
                    data: {
                        id: id
                    },
                    success: function(data) {
                        Swal.fire({
                            title: data.title,
                            text: data.text,
                            icon: data.icon,
                        }).then((Confirm) => {
                            refresh_data();
                        })
                    }
                })
            }
        })
    });

    $(document).on('click', '.btn-edit', function() {
        var id = $(this).data('id');
        window.location.href = "{{ url('admin/katalog/') }}" + '/' + id + '/edit';
    })
</script>
@endpush