@extends('layouts.master')

@push('css')
<link href="{{ asset('adminbite/assets/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.css" rel="stylesheet" />
@endpush

@section('bc')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Katalog</h4>
            <div class="d-flex align-items-center">

            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{ url('admin') }}">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="{{ url('admin/katalog') }}">Katalog</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Edit</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <form action="{{ url('admin/katalog/editData') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-8">
                    <input type="hidden" id="id" name="id" value="{{ $data->id }}">
                    <div class="form-group">
                        <label for="title">Judul</label>
                        <input type="text" name="title" id="title" class="form-control" placeholder="Judul" required value="{{ $data->title }}">
                    </div>
                    @foreach($data->authors as $authordata)
                    <div class="form-group">
                        @if($loop->first)
                        <label for="author">Author</label>
                        @endif
                        <div class="input-group" style="width: 100%;">
                            <select class="custom-select author-select" id="author" name="author_id[]">
                                <option value="0">Pilih Author</option>
                                @foreach($authors as $author)
                                <option value="{{ $author->id }}" @if($author->id==$authordata->id) selected='selected'
                                    @endif>{{ $author->name }} ({{ $author->email }})</option>
                                @endforeach
                            </select>
                            @if($loop->first)
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" id="add-author" type="button">Tambah
                                    Author</button>
                            </div>
                            @else
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary min-author" type="button">Hapus
                                    Author</button>
                            </div>
                            @endif
                        </div>
                    </div>
                    @endforeach
                    <div id="field-new-author"></div>
                    <div class="form-group">
                        <label for="abstract">Abstrak</label>
                        <textarea name="abstract" id="abstract" rows="4" class="form-control" placeholder="Abstrak">{{ $data->abstract }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="description">Deskripsi</label>
                        <textarea name="description" id="description" rows="4" class="form-control" placeholder="Deskripsi">{{ $data->description }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="keywords">Keywords</label>
                        <input type="text" name="keywords" id="keywords" class="form-control mb-2" placeholder="Keywords 1, Keywords 2, ..." required value="{{ $data->keywords }}">
                        <small class="text-mutted">Pisahkan dengan tanda koma.</small>
                    </div>
                    <div class="form-group">
                        <label for="file">File</label>
                        <input type="file" name="file" id="file" class="form-control" placeholder="File">
                        @error('file')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        <a class="mt-3 btn btn-outline-primary" target="_blank" href="{{ url('file/' . $data->download_path) }}">{{ $data->download_path }}</a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="">Simpan data</label>
                        <button class="btn btn-info btn-block">Simpan Data</button>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label for="type_id">Tipe Material</label>
                        <select name="type_id" id="type_id" class="form-control" required placeholder="Tipe Material">
                            <option value="0">Pilih Tipe</option>
                            @foreach($types as $type)
                            <option value="{{ $type->id }}" @if($type->id == $data->type_id) selected='selected'
                                @endif>{{ $type->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="category_id">Kategori</label>
                        <select name="category_id" id="category_id" class="form-control" required placeholder="Kategori">
                            <option value="0">Pilih kategori</option>
                            @foreach($categories as $category)
                            <option value="{{ $category->id }}" @if($category->id == $data->category_id)
                                selected='selected'
                                @endif>{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="isbn">ISBN</label>
                        <input type="text" name="isbn" id="isbn" class="form-control" placeholder="ISBN" value="{{ $data->isbn }}">
                    </div>
                    <div class="form-group">
                        <label for="edition">Edisi</label>
                        <input type="text" name="edition" id="edition" class="form-control" placeholder="Edisi" value="{{ $data->edition }}">
                    </div>
                    <div class="form-group">
                        <label for="volume">Volume</label>
                        <input type="text" name="volume" id="volume" class="form-control" placeholder="Volume" value="{{ $data->volume }}">
                    </div>
                    <div class="form-group">
                        <label for="number">Nomor</label>
                        <input type="text" name="number" id="number" class="form-control" placeholder="Nomor" value="{{ $data->number }}">
                    </div>
                    <div class="form-group">
                        <label for="publisher">Publisher</label>
                        <input type="text" name="publisher" id="publisher" class="form-control" placeholder="Publisher" value="{{ $data->publisher }}">
                    </div>
                    <div class="form-group">
                        <label for="year">Tahun Rilis</label>
                        <input type="month" name="year" id="year" class="form-control" placeholder="Tahun Rilis" required value="{{ \Carbon\Carbon::parse($data->year)->format('Y-m') }}">
                    </div>
                    <div class="form-group">
                        <label for="cover">Cover</label>
                        <div class="text-center my-3">
                            <img src="{{ url('cover/' . $data->cover_path) }}" alt="Cover" class="img-fluid">
                        </div>
                        <input type="file" name="cover" id="cover" class="form-control">
                        @error('cover')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@push('js')
<script src="{{ asset('adminbite/assets/extra-libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script>
    function refresh_data() {
        $('#dataTable').DataTable();
    }

    $(document).ready(function() {
        refresh_data();
    });

    $('.author-select2').select2({
        ajax: {
            url: "{{ url('admin/author/getAuthor') }}",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "post",
            dataType: 'json',
            delay: 250,
            data: function(params) {
                return {
                    searchTerm: params.term
                };
            },
            processResults: function(response) {
                return {
                    results: response
                };
            },
            cache: true
        },
        theme: "bootstrap"
    });

    $('#add-author').on('click', function() {
        $('#field-new-author').append(
            '<div class="form-group">' +
            '<div class="input-group" style="width: 100%;">' +
            '<select class="custom-select author-select" name="author_id[]">' +
            '<option value="0">Pilih Author</option>' +
            @foreach($authors as $author)
            '<option value="{{ $author->id }}">{{ $author->name }} ({{ $author->email }})</option>' +
            @endforeach '</select>' +
            '<div class="input-group-append">' +
            '<button class="btn btn-outline-secondary min-author" type="button">Kurangi' +
            ' Author</button>' +
            '</div>' +
            '</div>' +
            '</div>'
        );

    });

    $(document).on('click', '.min-author', function() {
        $(this).parent('div').parent('div').remove();
    });
</script>
@endpush