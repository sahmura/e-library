@extends('layouts.master')

@push('css')
<link href="{{ asset('adminbite/assets/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}"
    rel="stylesheet">
@endpush

@section('bc')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Pengguna</h4>
            <div class="d-flex align-items-center">

            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{ url('admin') }}">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Pengguna</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped" id="dataTable">
                        <thead>
                            <th style="width: 20px;text-align: center;">No</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Department</th>
                            <th>Akses</th>
                            <th style="width: 50px; text-align:center;"><i class="mdi mdi-arrange-bring-forward"></i>
                            </th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="roleModal" tabindex="-1" role="dialog" aria-labelledby="roleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="roleModalLabel">Detail pengguna</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h6 id="username"></h6>
                <p class="mb-3" id="useremail"></p>
                <form action="" method="POST" id="userForm">
                    @csrf
                    <input type="hidden" id="iduser" name="id">
                    <div class="form-group">
                        <label for="role">Hak Akses</label>
                        <select name="role" id="role" class="form-control">
                            <option value="admin">Admin</option>
                            <option value="operator">Operator</option>
                            <option value="student">Student</option>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-save-role">Simpan Data</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script src="{{ asset('adminbite/assets/extra-libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script>
    function refresh_data() {
        $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            destroy: true,
            ajax: {
                url: "{{ url('admin/user') }}",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'GET',
            },
            columns: [{
                    data: 'DT_RowIndex',
                    searchable: false,
                    class: 'text-center',
                    width: '20px'
                },
                {
                    data: 'name'
                },
                {
                    data: 'email'
                },
                {
                    data: 'department'
                },
                {
                    data: 'role'
                },
                {
                    data: 'action',
                    searchable: false,
                    class: 'text-center',
                    width: '20px'
                },
            ]
        });
    }

    $(document).ready(function () {
        refresh_data();
    });

    $(document).on('click', '.btn-detail', function () {
        $('#iduser').val($(this).data('id'));
        $('#username').html($(this).data('name'));
        $('#useremail').html($(this).data('email'));
        $('#role').val($(this).data('role'));
        $('#roleModal').modal('show');
    });

    $('.btn-save-role').on('click', function () {
        var data = $('#userForm').serialize();
        $.ajax({
            url: "{{ url('admin/user/changeRole') }}",
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: data,
            success: function (res) {
                $('#roleModal').modal('hide');
                Swal.fire({
                    title: res.title,
                    text: res.text,
                    type: res.icon,
                }).then((Confirm) => {
                    refresh_data();
                });
            }
        });
    })

</script>
@endpush
