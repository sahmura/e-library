@extends('layouts.master')

@push('css')
<link href="{{ asset('adminbite/assets/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">
@endpush

@section('bc')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Peminjaman</h4>
            <div class="d-flex align-items-center">

            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{ url('admin') }}">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Peminjaman</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="card bg-info text-white">
            <div class="card-body">
                <div class="d-flex no-block align-items-center">
                    <a href="JavaScript: void(0);"><i class="display-6 mdi mdi-account-convert text-white" title="ETH"></i></a>
                    <div class="m-l-15 m-t-10">
                        <h4 class="font-medium m-b-0">Peminjam Hari Ini</h4>
                        <h5>{{ count($data['peminjamHariIni']) }}</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card bg-success text-white">
            <div class="card-body">
                <div class="d-flex no-block align-items-center">
                    <a href="JavaScript: void(0);"><i class="display-6 mdi mdi-book-plus text-white" title="ETH"></i></a>
                    <div class="m-l-15 m-t-10">
                        <h4 class="font-medium m-b-0">Total Pinjaman Hari Ini</h4>
                        <h5>{{ $data['totalPinjamanHariIni'] }}</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive mt-5">
                    <table class="table table-striped" id="dataTable">
                        <thead>
                            <th style="width: 20px;text-align: center;">No</th>
                            <th>Nama Peminjam</th>
                            <th>Judul</th>
                            <th>Tanggal Pinjam</th>
                            <th>Batas Pinjam</th>
                            <th>Status</th>
                            <th>Keterangan</th>
                            <th style="width: 50px; text-align:center;"><i class="mdi mdi-arrange-bring-forward"></i>
                            </th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="detailModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="detailModalLabel">Detail Peminjaman</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h4 id="det_nama">Nama Peminjam</h4>
                <p class="lead" id="det_prodi">Prodi peminjam</p>
                <table class="table table-striped mt-3">
                    <tr>
                        <td colspan="2" id="det_material">Nama Material</td>
                    </tr>
                    <tr>
                        <td style="font-weight: 600;">Tanggal Pinjam</td>
                        <td style="font-weight: 600;">Tanggal Kembali</td>
                    </tr>
                    <tr>
                        <td id="det_awal">Tanggal Pinjam</td>
                        <td id="det_akhir">Tanggal Kembali</td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script src="{{ asset('adminbite/assets/extra-libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script>
    function refresh_data() {
        $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            destroy: true,
            ajax: {
                url: "{{ url('admin/peminjaman') }}",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'GET',
            },
            columns: [{
                    data: 'DT_RowIndex',
                    searchable: false,
                    class: 'text-center',
                    width: '20px'
                },
                {
                    data: 'peminjam'
                },
                {
                    data: 'material'
                },
                {
                    data: 'tgl_pinjam'
                },
                {
                    data: 'tgl_kembali'
                },
                {
                    data: 'status'
                },
                {
                    data: 'keterangan'
                },
                {
                    data: 'action',
                    searchable: false,
                    class: 'text-center',
                    width: '20px'
                },
            ]
        });
    }

    $(document).ready(function() {
        refresh_data();
    });

    $(document).on('click', '.btn-detail', function() {
        $('#det_nama').html($(this).data('name'));
        $('#det_prodi').html($(this).data('prodi'));
        $('#det_material').html($(this).data('material'));
        $('#det_awal').html($(this).data('tglpinjam'));
        $('#det_akhir').html($(this).data('tglkembali'));
        $('#detailModal').modal('show');
    });

    $(document).on('click', '.btn-delete', function() {
        swal.fire({
            title: 'Apakah anda akan membatalkan peminjaman?',
            text: 'Ketika anda membatalkan peminjaman, pengguna harus mengulangi peminjaman yang dilakukan',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Batalkan",
        }).then((Confirm) => {
            if (Confirm.value) {
                $.ajax({
                    url: "{{ url('admin/peminjaman/cancel') }}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: 'POST',
                    data: {
                        id: $(this).data('id')
                    },
                    success: function(data) {
                        swal.fire({
                            title: data.title,
                            text: data.text,
                            type: data.type,
                        }).then((Confirm) => {
                            refresh_data();
                        });
                    }
                })
            }
        });
    })
</script>
@endpush