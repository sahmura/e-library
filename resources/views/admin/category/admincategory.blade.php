@extends('layouts.master')

@push('css')
<link href="{{ asset('adminbite/assets/extra-libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}"
    rel="stylesheet">
@endpush

@section('bc')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Kelola Kategori</h4>
            <div class="d-flex align-items-center">

            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{ url('admin') }}">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Kategori</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <button class="btn btn-info float-right mb-3" onclick="addData()">Tambah Data</button>
                <div class="table-responsive">
                    <table class="table table-striped" id="dataTable">
                        <thead>
                            <th style="width: 20px;text-align: center;">No</th>
                            <th>Nama</th>
                            <th style="width: 50px; text-align:center;"><i class="mdi mdi-arrange-bring-forward"></i>
                            </th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="dataModal" tabindex="-1" role="dialog" aria-labelledby="dataModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="dataModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="POST" id="dataForm">
                    @csrf
                    <input type="hidden" id="id" name="id">
                    <div class="form-group">
                        <label for="name">Nama Kategori</label>
                        <input type="text" name="name" id="name" class="form-control" placeholder="Nama Kategori"
                            required>
                    </div>
                    <div class="form-group">
                        <label for="description">Deskripsi</label>
                        <textarea name="description" id="description" rows="4" placeholder="Deskripsi" required
                            class="form-control"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn" id="btn-simpan">Simpan data</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script src="{{ asset('adminbite/assets/extra-libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script>
    function refresh_data() {
        $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            destroy: true,
            ajax: {
                url: "{{ url('admin/category') }}",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'GET',
            },
            columns: [{
                    data: 'DT_RowIndex',
                    searchable: false,
                    class: 'text-center',
                    width: '20px'
                },
                {
                    data: 'name'
                },
                {
                    data: 'action',
                    searchable: false,
                    class: 'text-center',
                    width: '20px'
                },
            ]

        });
    }

    function addData() {
        $('.modal-title').html('Tambah Data');
        $('#dataForm')[0].reset();
        $('#btn-simpan').removeClass('btn-warning');
        $('#btn-simpan').addClass('btn-info');
        $('#dataModal').modal('show');
    }

    $(document).ready(function () {
        refresh_data();
    });

    $('#btn-simpan').on('click', function () {
        var data = $("#dataForm").serialize();
        if ($('#id').val() == '') {
            var url = "{{ url('admin/category/addData') }}";
        } else {
            var url = "{{ url('admin/category/editData') }}";
        }
        $.ajax({
            url: url,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: 'POST',
            data: data,
            success: function (data) {
                $('#dataModal').modal('hide');
                Swal.fire({
                    title: data.title,
                    text: data.text,
                    type: data.type,
                }).then((Confirm) => {
                    refresh_data();
                });
            }
        });
    });

    $(document).on('click', '.btn-edit', function () {
        $('.modal-title').html('Edit Data');
        $('#dataForm')[0].reset();
        $('#btn-simpan').removeClass('btn-info');
        $('#btn-simpan').addClass('btn-warning');
        $('#id').val($(this).data('id'));
        $('#name').val($(this).data('name'));
        $('#description').val($(this).data('description'));
        $('#dataModal').modal('show');
    });

    $(document).on('click', '.btn-delete', function () {
        var id = $(this).data('id');
        Swal.fire({
            title: 'Apakah Anda Yakin?',
            text: 'Semua katalog akan dipindahkan ke kategori "Tanpa Kategori"',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Hapus",
        }).then((Confirm) => {
            if (Confirm.value) {
                $.ajax({
                    url: "{{ url('admin/category/deleteData') }}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    method: 'POST',
                    data: {
                        id: id
                    },
                    success: function (data) {
                        Swal.fire({
                            title: data.title,
                            text: data.text,
                            type: data.type,
                        }).then((Confirm) => {
                            refresh_data();
                        });
                    }
                });
            }
        })
    });

</script>
@endpush
