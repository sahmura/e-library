@extends('layouts.master')

@push('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.css">
@endpush

@section('bc')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Statistik</h4>
            <div class="d-flex align-items-center">

            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{ url('admin') }}">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Statistik</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="fix-width">
    <div class="container-fluid">
        <div class="row mb-5">
            <div class="col-md-12">
                <h4>Statistik Pustaka</h4>
                <hr>
            </div>
            <div class="col-md-6 mb-3">
                <div class="card">
                    <div class="card-body">
                        <button class="btn btn-success float-right" id="getTotalByCategory-button">Tampilkan</button>
                        <h4 class="card-title">Total Per Kategori</h4>
                        <div class="mt-3">
                            <canvas id="getTotalByCategory"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-3">
                <div class="card">
                    <div class="card-body">
                        <button class="btn btn-success float-right" id="getTotalByType-button">Tampilkan</button>
                        <h4 class="card-title">Total Per Tipe</h4>
                        <div class="mt-3">
                            <canvas id="getTotalByType"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-3">
                <div class="card">
                    <div class="card-body">
                        <button class="btn btn-success float-right" id="getTotalByYear-button">Tampilkan</button>
                        <h4 class="card-title">Total Per Tahun Terbit</h4>
                        <div class="mt-3">
                            <canvas id="getTotalByYear"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-3">
                <div class="card">
                    <div class="card-body">
                        <button class="btn btn-success float-right" id="getTotalPinjamByYear-button">Tampilkan</button>
                        <h4 class="card-title">Total Pinjaman</h4>
                        <div class="mt-3">
                            <canvas id="getTotalPinjamByYear"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-3">
                <div class="card">
                    <div class="card-body">
                        <button class="btn btn-success float-right" id="getTotalUnduhByYear-button">Tampilkan</button>
                        <h4 class="card-title">Total Unduhan</h4>
                        <div class="mt-3">
                            <canvas id="getTotalUnduhByYear"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row my-5">
            <div class="col-md-12">
                <h4>Statistik Pengguna</h4>
                <hr>
            </div>
            <div class="col-md-6 mb-3">
                <div class="card">
                    <div class="card-body">
                        <button class="btn btn-success float-right" id="getTotalUserByProdiM-button">Tampilkan</button>
                        <h4 class="card-title">Total Pengguna Per Prodi S2</h4>
                        <div class="mt-3 overflow-auto">
                            <canvas id="getTotalUserByProdiM"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-3">
                <div class="card">
                    <div class="card-body">
                        <button class="btn btn-success float-right" id="getTotalUserByProdiS-button">Tampilkan</button>
                        <h4 class="card-title">Total Pengguna Per Prodi S3</h4>
                        <div class="mt-3">
                            <canvas id="getTotalUserByProdiS"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@push('js')
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>
<script>
    $('#getTotalPinjamByYear-button').on('click', function() {
        $.ajax({
            url: "{{ url('statistik/getTotalPinjamByYear') }}",
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                var label = data.label;
                var value = data.value;
                var type = data.type;
                var color = data.color;
                var ctx = document.getElementById('getTotalPinjamByYear').getContext('2d');
                var getTotalPinjamByYear = new Chart(ctx, {
                    type: type,
                    data: {
                        labels: label,
                        datasets: [{
                            data: value,
                            label: 'Total Pinjaman',
                            backgroundColor: color[0],
                        }]
                    },
                    options: {
                        responsive: true,
                        legend: {
                            position: 'bottom'
                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    min: 0, // it is for ignoring negative step.
                                    beginAtZero: true,
                                    stepSize: 1
                                }
                            }]
                        },
                    }
                })
            }
        });
    });

    $('#getTotalUnduhByYear-button').on('click', function() {
        $.ajax({
            url: "{{ url('statistik/getTotalUnduhByYear') }}",
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                var label = data.label;
                var value = data.value;
                var type = data.type;
                var color = data.color;
                var ctx = document.getElementById('getTotalUnduhByYear').getContext('2d');
                var getTotalUnduhByYear = new Chart(ctx, {
                    type: type,
                    data: {
                        labels: label,
                        datasets: [{
                            data: value,
                            label: 'Total Unduhan',
                            backgroundColor: color[0],
                        }]
                    },
                    options: {
                        responsive: true,
                        legend: {
                            position: 'bottom'
                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    min: 0, // it is for ignoring negative step.
                                    beginAtZero: true,
                                    stepSize: 1
                                }
                            }]
                        },
                    }
                })
            }
        });
    });

    $('#getTotalUserByProdiS-button').on('click', function() {
        $.ajax({
            url: "{{ url('statistik/getTotalUserByProdiS') }}",
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                var label = data.label;
                var value = data.value;
                var type = data.type;
                var color = data.color;
                var ctx = document.getElementById('getTotalUserByProdiS').getContext('2d');
                var getTotalUserByProdiS = new Chart(ctx, {
                    type: type,
                    data: {
                        labels: label,
                        datasets: [{
                            data: value,
                            label: 'Total Pengguna Jenjang S1',
                            backgroundColor: color,
                        }]
                    },
                    options: {
                        responsive: true,
                        legend: {
                            position: 'bottom',
                            display: false
                        }
                    }
                })
            }
        });
    });

    $('#getTotalUserByProdiM-button').on('click', function() {
        $.ajax({
            url: "{{ url('statistik/getTotalUserByProdiM') }}",
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                var label = data.label;
                var value = data.value;
                var type = data.type;
                var color = data.color;
                var ctx = document.getElementById('getTotalUserByProdiM').getContext('2d');
                new Chart(ctx, {
                    type: type,
                    data: {
                        datasets: [{
                            data: value,
                            backgroundColor: color,
                            label: 'Total Pengguna Jenjang S2'
                        }],
                        labels: label
                    },
                    options: {
                        responsive: true,
                        legend: {
                            position: 'bottom',
                            display: false,
                        }
                    }
                })
            }
        });
    });

    $('#getTotalByYear-button').on('click', function() {
        $.ajax({
            url: "{{ url('statistik/getTotalByYear') }}",
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                var label = data.label;
                var value = data.value;
                var type = data.type;
                var color = data.color;
                var ctx = document.getElementById('getTotalByYear').getContext('2d');
                new Chart(ctx, {
                    type: type,
                    data: {
                        datasets: [{
                            data: value,
                            backgroundColor: color,
                            label: 'Total Per Tahun Terbit'
                        }],
                        labels: label
                    },
                    options: {
                        responsive: true,
                        legend: {
                            position: 'bottom',
                            display: false,
                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    min: 0, // it is for ignoring negative step.
                                    beginAtZero: true,
                                    stepSize: 1
                                }
                            }]
                        },
                    }
                })
            }
        });
    });

    $('#getTotalByType-button').on('click', function() {
        $.ajax({
            url: "{{ url('statistik/getTotalByType') }}",
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                var label = data.label;
                var value = data.value;
                var type = data.type;
                var color = data.color;
                var ctx = document.getElementById('getTotalByType').getContext('2d');
                new Chart(ctx, {
                    type: type,
                    data: {
                        datasets: [{
                            data: value,
                            backgroundColor: color,
                            label: 'Total Per Tipe'
                        }],
                        labels: label
                    },
                    options: {
                        responsive: true,
                        legend: {
                            position: 'bottom',
                            display: false,
                        }
                    }
                })
            }
        });
    });

    $('#getTotalByCategory-button').on('click', function() {
        $.ajax({
            url: "{{ url('statistik/getTotalByCategory') }}",
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                var label = data.label;
                var value = data.value;
                var type = data.type;
                var color = data.color;
                var ctx = document.getElementById('getTotalByCategory').getContext('2d');
                new Chart(ctx, {
                    type: type,
                    data: {
                        datasets: [{
                            data: value,
                            backgroundColor: color,
                            label: 'Total Per Kategori'
                        }],
                        labels: label
                    },
                    options: {
                        responsive: true,
                        legend: {
                            position: 'bottom',
                            display: false,
                        }
                    }
                })
            }
        });
    });
</script>
@endpush