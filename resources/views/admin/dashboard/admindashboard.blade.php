@extends('layouts.master')

@section('bc')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Dashboard</h4>
            <div class="d-flex align-items-center">

            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="#">Dashboard</a>
                        </li>
                        <!-- <li class="breadcrumb-item active" aria-current="page">Library</li> -->
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-3">
        <div class="card bg-cyan text-white">
            <div class="card-body">
                <div class="d-flex no-block align-items-center">
                    <a href="JavaScript: void(0);"><i class="display-6 mdi mdi-archive text-white" title="Arsip"></i></a>
                    <div class="m-l-15 m-t-10">
                        <h4 class="font-medium m-b-0">Total Arsip</h4>
                        <h5>{{ $data['totalData'] }}</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card bg-warning text-white">
            <div class="card-body">
                <div class="d-flex no-block align-items-center">
                    <a href="JavaScript: void(0);"><i class="display-6 mdi mdi-account text-white" title="User"></i></a>
                    <div class="m-l-15 m-t-10">
                        <h4 class="font-medium m-b-0">Total User</h4>
                        <h5>{{ $data['totalUser'] }}</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card bg-success text-white">
            <div class="card-body">
                <div class="d-flex no-block align-items-center">
                    <a href="JavaScript: void(0);"><i class="display-6 mdi mdi-content-save-all text-white" title="Pinjaman"></i></a>
                    <div class="m-l-15 m-t-10">
                        <h4 class="font-medium m-b-0">Total Pinjaman</h4>
                        <h5>{{ $data['totalPinjaman'] }}</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card bg-info text-white">
            <div class="card-body">
                <div class="d-flex no-block align-items-center">
                    <a href="JavaScript: void(0);"><i class="display-6 mdi mdi-download text-white" title="Download"></i></a>
                    <div class="m-l-15 m-t-10">
                        <h4 class="font-medium m-b-0">Total Download</h4>
                        <h5>{{ $data['totalDownloads'] }}</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                Publish Terbaru
            </div>
            <div class="card-body">
                <ul class="list-unstyled">
                    @foreach($lastPublish as $last)
                    <li class="media mb-3">
                        <img class="d-flex mr-3" src="{{ url('cover/' . $last->cover_path) }}" width="70" alt="Cover say it do it">
                        <div class="media-body pl-2">
                            <h5 class="mt-0 mb-1" style="font-weight: 600;">{{ $last->title }}</h5>
                            <p class="lead">{{ $last->isbn }}</p>
                            Total
                        </div>
                    </li>
                    <hr>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                Populer
            </div>
            <div class="card-body">
                <ul class="list-unstyled">
                    @foreach($populers as $populer)
                    <li class="media mb-3">
                        <img class="d-flex mr-3" src="{{ url('cover/' . $populer->cover_path) }}" width="70" alt="Cover say it do it">
                        <div class="media-body pl-2">
                            <h5 class="mt-0 mb-1" style="font-weight: 600;">{{ $populer->title }}</h5>
                            <p class="lead">{{ $populer->isbn }}</p>
                            Total
                        </div>
                    </li>
                    <hr>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection