@extends('layouts.master')
@push('css')
<style>
    .embed-responsive .card-img-top {
        object-fit: cover !important;
    }
</style>
@endpush

@section('bc')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Eksplore</h4>
            <div class="d-flex align-items-center">

            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="#">Eksplore</a>
                        </li>
                        <!-- <li class="breadcrumb-item active" aria-current="page">Library</li> -->
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-2 col-sm-6">
        <div class="form-group">
            <select name="type_id" id="type_id" class="form-control">
                <option value="all">Filter Tipe</option>
                @foreach($types as $type)
                <option value="{{ urlencode($type->name) }}">{{ $type->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-2 col-sm-6">
        <div class="form-group">
            <select name="category_id" id="category_id" class="form-control">
                <option value="all">Filter Kategori</option>
                @foreach($categories as $category)
                <option value="{{ urlencode($category->name) }}">{{ $category->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-2 col-sm-12">
        <div class="form-group">
            <select name="tahun" id="tahun" class="form-control">
                <option value="all">Tahun</option>
                @for($tahun = \Carbon\Carbon::now()->year; $tahun >= 1900 ; $tahun--)
                <option value="{{ $tahun }}">{{ $tahun }}</option>
                @endfor
            </select>
        </div>
    </div>
    <div class="col-md-4 col-sm-12">
        <div class="form-group">
            <input type="text" name="key" id="key" placeholder="Masukan judul atau keywords" class="form-control">
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group text-right">
            <button class="btn btn-info" id="btn-filter">Filter data</button>
        </div>
    </div>
</div>
<div class="row mt-5">
    <div class="col-md-12">
        <h5>Baru ditambahkan</h5>
        <hr>
    </div>
</div>
<div class="row">
    @if($materials->isNotEmpty())
    @foreach($materials as $material)
    <div class="col-md-2">
        <div class="card h-100">
            <div class="embed-responsive embed-responsive-1by1">
                <img class="card-img-top embed-responsive-item" src="{{ url('cover/' . $material->cover_path) }}" alt="{{ $material->title }}"></div>
            <div class="card-body">
                <div class="d-flex no-block align-items-center m-b-5">
                    <!-- <span><i class="ti-calendar mr-2"></i>{{ \Carbon\Carbon::parse($material->year)->year }}</span> -->
                    <div class="ml-auto">
                        <!-- <a href="javascript:void(0)" class="link"><i class="ti-comments"></i> 3 Comments</a> -->
                    </div>
                </div>
                <!-- <h3 class="font-normal">{{ $material->title }}</h3> -->
                <p class="m-b-0 m-t-5 text-truncate">
                    <a href="{{ url('dashboard/explore/' . $material->id . '/detail') }}" class="">{{ $material->title }}</a>
                </p>
            </div>
        </div>
    </div>
    @endforeach
    @else
    <div class="col-md-12">
        <h5 class="text-center">Belum ada data</h5>
    </div>
    @endif
</div>
<div class="row">
    <div class="col-md-12 text-center">
        {{ $materials->links() }}
    </div>
</div>
@endsection
@push('js')
<script>
    $('#btn-filter').on('click', function() {
        var type = $('#type_id').val();
        var category = $('#category_id').val();
        var tahun = $('#tahun').val();
        var key = encodeURI($('#key').val());
        window.location.href = "{{ url('dashboard/explore/search/') }}" + '/' + type + '/' + category + '/' +
            tahun + '/' + key;
    })
</script>
@endpush