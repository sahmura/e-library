@extends('layouts.master')

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('adminbite/assets/libs/pickadate/lib/themes/default.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('adminbite/assets/libs/pickadate/lib/themes/default.date.css') }}">
@endpush

@section('bc')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Eksplore</h4>
            <div class="d-flex align-items-center">

            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{ url('dashboard/explore') }}">Eksplore</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $material->title }}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <h3>{{ $material->title }}</h3>
        <hr>
    </div>
</div>
<div class="row">
    <div class="col-md-12 mb-3">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3">
                        <img class="img-responsive img-fluid" src="{{ url('cover/' . $material->cover_path) }}" alt="{{ $material->title }}">
                    </div>
                    <div class="col-md-6">
                        <table class="table table-borderles">
                            <tr>
                                <td>Judul</td>
                                <td>{{ $material->title }}</td>
                            </tr>
                            <tr>
                                <td>ISBN</td>
                                <td>{{ $material->isbn }}</td>
                            </tr>
                            <tr>
                                <td>Edisi</td>
                                <td>{{ $material->edition }}</td>
                            </tr>
                            <!-- <tr>
                                <td>Volume</td>
                                <td>{{ $material->volume }}</td>
                            </tr>
                            <tr>
                                <td>Number</td>
                                <td>{{ $material->number }}</td>
                            </tr> -->
                            <tr>
                                <td>Publisher</td>
                                <td>{{ $material->publisher }}</td>
                            </tr>
                            <tr>
                                <td>Tahun Rilis</td>
                                <td>{{ \Carbon\Carbon::parse($material->year)->locale('id')->isoFormat('MMMM YYYY') }}
                                </td>
                            </tr>
                            <tr>
                                <td>Kategori</td>
                                <td>{{ $material->category->name }}
                                </td>
                            </tr>
                            <tr>
                                <td>Keywords</td>
                                <td>{{ $material->keywords}}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 mb-3">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-2">
                        Dibaca<br>
                        {{ $totalPinjam }} Kali
                    </div>
                    <div class="col-md-2">
                        Diunduh <br>
                        {{ $material->downloads }} Kali
                    </div>
                    <div class="col-md-8 text-right">
                        @if($isBorrowed == 0)
                        <button class="btn waves-effect waves-light btn-outline-primary" onclick="pinjamModal()"><i class="mdi mdi-content-save"></i> Pinjam</button>
                        @endif
                        <button class="btn btn-success btn-outline" onclick="downloadModal()"><i class="mdi mdi-download"></i> Unduh</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 mb-3">
        <div class="card">
            <div class="card-body">
                @if($material->abstract != '')
                <h5>Abstrak</h5>
                {{ $material->abstract }}
                @endif
                @if($material->description != '')
                <h5 class="mt-3">Deskripsi</h5>
                {{ $material->description }}
                @endif
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="transactionModal" tabindex="-1" role="dialog" aria-labelledby="transactionModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="transactionModalLabel">Pinjam Material</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-2">
                        <img class="img-responsive img-fluid" src="{{ url('cover/' . $material->cover_path) }}" alt="{{ $material->title }}">
                    </div>
                    <div class="col-md-10">
                        <h5>{{ $material->title }}</h5>
                        @if($material->isbn != '')
                        <p>{{ $material->isbn }}</p>
                        @endif
                        <p>
                            {{ ($material->edition) ? 'Edisi ' . $material->edition : '' }},
                            {{ ($material->volume) ? 'Volume ' . $material->volume : '' }},
                            {{ ($material->number) ? 'Edisi ' . $material->number : '' }}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <form action="" method="POST" id="dataForm">
                            <input type="hidden" id="id" name="id" value="{{ $material->id }}">
                            <input type="hidden" id="typeTransaction" name="typeTransaction">
                            <div class="form-group">
                                <label for="tanggal_pinjam">Tanggal Pinjam</label>
                                <input type="text" name="tanggal_pinjam" id="tanggal_pinjam" class="form-control datepick" required value="{{ \Carbon\Carbon::now()->format('Y-m-d') }}" placeholder="Tanggal Pinjam">
                            </div>
                            <div class="form-group">
                                <label for="tanggal_kembali">Tanggal Kembali</label>
                                <input type="text" name="tanggal_kembali" id="tanggal_kembali" class="form-control datepick" required value="{{ \Carbon\Carbon::tomorrow()->format('Y-m-d') }}">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn-transaction">Proses Transaksi</button>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script src="{{ asset('adminbite/assets/libs/pickadate/lib/compressed/picker.js') }}   "></script>
<script src="{{ asset('adminbite/assets/libs/pickadate/lib/compressed/picker.date.js') }}  "></script>
<script>
    function pinjamModal() {
        $('#dataForm')[0].reset();
        $('#transactionModalLabel').html('Pinjam Material');
        $('#typeTransaction').val('PINJAM');
        $('#transactionModal').modal('show');
    }

    function downloadModal() {
        $('#dataForm')[0].reset();
        $('#transactionModalLabel').html('Unduh Material');
        $('#typeTransaction').val('UNDUH');
        $('#transactionModal').modal('show');
    }

    $('#btn-transaction').on('click', function() {
        var data = $('#dataForm').serialize();
        var type = $('#typeTransaction').val();
        if (type == 'UNDUH') {
            var url = "{{ url('dashboard/transaction/download') }}";
        } else if (type == 'PINJAM') {
            var url = "{{ url('dashboard/transaction/pinjam') }}";
        }
        $.ajax({
            url: url,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            method: 'POST',
            data: data,
            success: function(data) {
                Swal.fire({
                    title: data.title,
                    text: data.text,
                    type: data.icon,
                }).then((Confirm) => {
                    if (type == 'PINJAM') {
                        window.location.href = "{{ url('dashboard/my') }}";
                    } else if (type == 'UNDUH') {
                        window.location.href = "{{ url('dashboard/download/') }}" + '/' + $(
                            '#id').val();
                    }
                });
            }
        })
    })

    $(document).ready(function() {
        $('.datepick').pickadate({
            format: 'yyyy-mm-dd',
            formatSubmit: 'yyyy-mm-dd'
        });
    })
</script>
@endpush