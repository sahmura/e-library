@extends('layouts.master')

@push('css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.css"
    rel="stylesheet" />
@endpush

@section('bc')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Pengaturan</h4>
            <div class="d-flex align-items-center">

            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="#">Pengaturan</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $data->name }}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <h5>Pengaturan diri</h5>
            </div>
            <div class="card-body">
                <form action="" method="POST" id="data-diri-form">
                    @csrf
                    <div class="form-group">
                        <label for="name">Nama lengkap</label>
                        <input type="text" name="name" id="name" placeholder="Nama lengkap" value="{{ $data->name }}"
                            class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" id="email" placeholder="example@mail.com"
                            value="{{ $data->email }}" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="prody_id">Program Studi</label>
                        <select name="prody_id" id="prody_id" class="form-control select2" style="width: 100%;">
                            @foreach($prodies as $prodi)
                            <option value="{{ $prodi->id }}">[{{ $prodi->jenjang }}] {{ $prodi->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="telp">Telepon</label>
                        <input type="telp" name="telp" id="telp" placeholder="628..." value="{{ $data->telp }}"
                            class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <textarea type="alamat" name="alamat" id="alamat" placeholder="Alamat" class="form-control"
                            required>{{ $data->alamat }}</textarea>
                    </div>
                    <div class="form-group">
                        <button type="button" class="btn btn-success btn-block" id="btn-simpan-data-diri">Simpan data
                            diri</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <h5>Pengaturan kata sandi</h5>
            </div>
            <div class="card-body">
                <form action="" method="POST" id="pass-form">
                    @csrf
                    <div class="form-group">
                        <label for="oldpass">Kata Sandi Lama</label>
                        <input type="password" name="oldpass" id="oldpass" class="form-control"
                            placeholder="Kata sandi lama" required>
                    </div>
                    <div class="form-group">
                        <label for="password">Kata Sandi Baru</label>
                        <input type="password" name="password" id="password" class="form-control"
                            placeholder="Kata sandi baru" required>
                    </div>
                    <div class="form-group">
                        <label for="confirm-pass">Ulangi Kata Sandi</label>
                        <input type="password" name="confirm-pass" id="confirm-pass" class="form-control"
                            placeholder="Ulangi kata sandi" required>
                        <small id="error-confirm-pass" class="text-danger"></small>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-warning btn-block" type="button" id="btn-update-pass">Perbaharui kata
                            sandi</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script>
    $('#prody_id').select2({
        theme: "bootstrap"
    });
    $(document).ready(function () {
        $('#prody_id').val("{{ $data->prodi_id }}");
        $('#prody_id').trigger("change.select2");
    });

    $('#confirm-pass').on('keyup', function () {
        if ($(this).val() != $('#password').val()) {
            $('#error-confirm-pass').html('Kata sandi tidak sesuai');
            $(this).addClass('is-invalid');
            $('#password').addClass('is-invalid');
            $('#btn-update-pass').prop('disabled', true);
        } else {
            $('#error-confirm-pass').html('');
            $(this).removeClass('is-invalid');
            $('#password').removeClass('is-invalid');
            $('#btn-update-pass').prop('disabled', false);
        }
    })

    $('#btn-simpan-data-diri').on('click', function () {
        var data = $('#data-diri-form').serialize();
        $.ajax({
            url: "{{ url('dashboard/updateprofile') }}",
            method: 'POST',
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (res) {
                Swal.fire({
                    title: res.title,
                    text: res.text,
                    type: res.icon,
                }).then((Confirm) => {
                    window.location.reload();
                })
            }
        })
    });

    $('#btn-update-pass').on('click', function () {
        var data = $('#pass-form').serialize();
        $.ajax({
            url: "{{ url('dashboard/updatepassword') }}",
            method: 'POST',
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (res) {
                Swal.fire({
                    title: res.title,
                    text: res.text,
                    type: res.icon,
                }).then((Confirm) => {
                    window.location.reload();
                })
            }
        })
    })

</script>
@endpush
