@extends('layouts.master')

@section('bc')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Dashboard</h4>
            <div class="d-flex align-items-center">

            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="#">Dashboard</a>
                        </li>
                        <!-- <li class="breadcrumb-item active" aria-current="page">Library</li> -->
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@if (Auth()->user()->prodi_id == 0 || Auth()->user()->prodi_id == null)

@push('css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.css" rel="stylesheet" />
@endpush
@section('content')
<div class="row d-flex justify-content-center">
    <div class="col-md-6">
        <form action="{{ url('dashboard/updatePrody') }}" method="POST">
            @csrf
            <div class="form-group text-center">
                <h5 class="mb-3">Silahkan isi program studi saudara</h5>
                <select name="prody_id" id="prody_id" class="form-control" required style="width: 100%;">
                    @foreach($prodies as $prodi)
                    <option value="{{ $prodi->id }}">[{{ $prodi->jenjang }}] {{ $prodi->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-block btn-success" id="btn-simpan-prodi">Simpan data</button>
            </div>
        </form>
    </div>
</div>
@endsection
@push('js')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script>
    $('#prody_id').select2({
        theme: "bootstrap"
    });
</script>
@endpush

@else

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card bg-light no-card-border">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div class="m-r-10">
                        <img src="{{ asset('adminbite/assets/images/users/profile.svg') }}" alt="{{ Auth()->user()->name }}" width="60" class="rounded-circle" />
                    </div>
                    <div>
                        <h3 class="m-b-0">Hai, {{ Auth()->user()->name }}!</h3>
                        <span>{{ \Carbon\Carbon::now()->locale('id')->isoFormat('Do MMMM, YYYY') }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="card bg-info text-white">
            <div class="card-body">
                <div class="d-flex no-block align-items-center">
                    <a href="#"><i class="display-6 mdi mdi-download text-white" title="Download"></i></a>
                    <div class="m-l-15 m-t-10">
                        <h4 class="font-medium m-b-0">Material Dipinjam</h4>
                        <h5>{{ $data['materialDipinjam'] }}</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card bg-info text-white">
            <div class="card-body">
                <div class="d-flex no-block align-items-center">
                    <a href="#"><i class="display-6 mdi mdi-download text-white" title="Download"></i></a>
                    <div class="m-l-15 m-t-10">
                        <h4 class="font-medium m-b-0">Total Unduh</h4>
                        <h5>{{ $data['materialDiunduh'] }}</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                Sedang dipinjam
            </div>
            <div class="card-body">
                @if($materialsdipinjam->isNotEmpty())
                <ul class="list-unstyled">
                    @foreach($materialsdipinjam as $dipinjam)
                    <li class="media mb-3">
                        <img class="d-flex mr-3" src="{{ url('cover/' . $dipinjam->material->cover_path) }}" width="70" alt="Cover say it do it">
                        <div class="media-body pl-2">
                            <h5 class="mt-0 mb-1" style="font-weight: 600;">{{ $dipinjam->material->title }}</h5>
                            <p class="lead">
                                {{ ($dipinjam->material->isbn != '') ? 'ISBN : ' . $dipinjam->material->isbn : 'Edisi : ' . $dipinjam->material->edition }}
                            </p>
                            <!-- Total -->
                            <a href="{{ url('dashboard/read/' . $dipinjam->material_id) }}" class="btn btn-success btn-xs">Baca</a>
                        </div>
                    </li>
                    @endforeach
                    <hr>
                </ul>
                @else
                <h5 class="text-center">
                    Belum ada yang dipinjam
                </h5>
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                Terakhir dipinjam
            </div>
            <div class="card-body">
                @if($materials->isNotEmpty())
                <ul class="list-unstyled">
                    @foreach($materials as $material)
                    <li class="media mb-3">
                        <img class="d-flex mr-3" src="{{ url('cover/' . $material->material->cover_path) }}" width="70" alt="Cover say it do it">
                        <div class="media-body pl-2">
                            <h5 class="mt-0 mb-1" style="font-weight: 600;">{{ $material->material->title }}</h5>
                            <p class="lead">
                                {{ ($material->material->isbn != '') ? 'ISBN : ' . $material->material->isbn : 'Edisi : ' . $material->material->edition }}
                            </p>
                            <!-- Total -->
                        </div>
                    </li>
                    @endforeach
                    <hr>
                </ul>
                @else
                <h5 class="text-center">
                    Belum ada yang dipinjam
                </h5>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@endif