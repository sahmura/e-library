@extends('layouts.master')

@section('bc')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Dashboard</h4>
            <div class="d-flex align-items-center">

            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="#">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="{{ url('dashboard/my') }}">Kelola Pinjaman</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $material->material->title }}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
@if($extension == 'pdf')
<div class="row">
    <div class="col-lg-12">
        <div class="card bg-light no-card-border">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div>
                        <h3 class="mb-2">{{ $material->material->title }}</h3>
                        <p>
                            @foreach($material->material->authors as $author)
                            {{ $author->name }}
                            @if(!$loop->last)
                            ,
                            @endif
                            @endforeach
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
<div class="row">
    <div class="col-md-12 text-center">
        @switch($extension)
        @case('pdf')
        <div id="reader"></div>
        @break
        @default
        <img src="{{ url('file/' . $material->material->download_path) }}" alt="$material->material->title" class="img-fluid">
        @endswitch
    </div>
</div>
@if($extension != 'pdf')
<div class="row mt-3">
    <div class="col-lg-12">
        <div class="card bg-light no-card-border">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div>
                        <h3 class="mb-2">{{ $material->material->title }}</h3>
                        <h6>
                            @foreach($material->material->authors as $author)
                            Oleh {{ $author->name }}
                            @if(!$loop->last)
                            ,
                            @endif
                            @endforeach
                        </h6>
                        @if($extension != 'pdf')
                        <p>{{$material->material->description}}</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@endsection
@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfobject/2.1.1/pdfobject.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.5.207/pdf.min.js"></script>
<script>
    function setReader(page, query) {
        if (page != null) {
            var options = {
                height: '600px',
                pdfOpenParams: {
                    view: 'FitV',
                    page: page,
                }
            };
        }

        if (query != null) {
            var options = {
                height: '500px',
                pdfOpenParams: {
                    view: 'FitV',
                    search: query,
                }
            };
        }

        PDFObject.embed("{{ url('file/' . $material->material->download_path) }}",
            "#reader", options);
    }
    $(document).ready(function() {
        setReader(1, null);
    })

    $('#btn-page').on('click', function() {
        var page = $('#page-form').val();
        setReader(page, null);
    });

    $('#btn-query').on('click', function() {
        var query = $('#query-form').val();
        setReader(null, query);
    });

    $(function() {
        $('#secondaryDownload').hide();
    });
</script>
@endpush