@extends('layouts.master')

@section('bc')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Kelola Pinjaman</h4>
            <div class="d-flex align-items-center">

            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="#">Kelola Pinjaman</a>
                        </li>
                        <!-- <li class="breadcrumb-item active" aria-current="page">Library</li> -->
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h5>Pinjaman Ku</h5>
            </div>
            <div class="card-body">
                <div class="row">
                    @if($materials->isNotEmpty())
                    @foreach($materials as $material)
                    <div class="col-md-4 mb-5">
                        <h5>{{ $material->material->title }}</h5>
                        <div class="row">
                            <div class="col-md-4">
                                <img class="img-responsive img-fluid" src="{{ url('cover/' . $material->material->cover_path) }}" alt="{{ $material->material->title }}">
                            </div>
                            <div class="col-md-8">
                                <p><b>ISBN :</b> {{ $material->material->isbn }}<br>
                                    <b>Edisi : </b> {{ $material->material->edition }}<br>
                                    <b>Tahun :</b>
                                    {{ \Carbon\Carbon::parse($material->material->year)->locale('id')->isoFormat('MMMM YYYY') }}
                                </p>
                                <span class="badge badge-warning">Batas waktu :
                                    {{ \Carbon\Carbon::parse($material->due_date)->locale('id')->isoFormat('Do MMMM YYYY') }}</span>
                                <div class="mt-3">
                                    <a href="{{ url('dashboard/explore/' . $material->material_id . '/detail') }}" class="btn btn-primary btn-sm">Detail</a>
                                    <a href="{{ url('dashboard/read/' . $material->material_id) }}" class="btn btn-info btn-sm">Baca</a>
                                    <a href="{{ url('dashboard/download/' . $material->material_id) }}" class="btn btn-success btn-sm">Unduh</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @else
                    <div class="col-md-12">
                        <h5 class="text-center">Belum ada data</h5>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <h5>Bookmark</h5>
            </div>
            <div class="card-body">
                <div class="row">
                    @if(!empty($bookmarks))
                    @foreach($bookmarks as $material)
                    <div class="col-md-12 mb-5">
                        <h5>{{ $material->title }}</h5>
                        <div class="row">
                            <div class="col-md-4">
                                <img class="img-responsive img-fluid" src="{{ url('cover/' . $material->cover_path) }}" alt="{{ $material->title }}">
                            </div>
                            <div class="col-md-8">
                                <p><b>ISBN :</b> {{ $material->isbn }}<br>
                                    <b>Edisi : </b> {{ $material->edition }}<br>
                                    <b>Tahun :</b>
                                    {{ \Carbon\Carbon::parse($material->year)->locale('id')->isoFormat('MMMM YYYY') }}
                                </p>
                                <div class="mt-3">
                                    <button class="btn btn-primary btn-sm">Detail</button>
                                    <button class="btn btn-success btn-sm">Pinjam</button>
                                    <button class="btn btn-danger btn-sm">Hapus</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @else
                    <div class="col-md-12">
                        <h5 class="text-center">Belum ada data</h5>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div> -->
</div>
@endsection