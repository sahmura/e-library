@extends('layouts.app')
@push('css')
<style>
    .embed-responsive .card-img-top {
        object-fit: cover !important;
    }
</style>
@endpush

@section('content')
<div class="fix-width" style="min-height: 47vh;">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 col-sm-6">
                <div class="form-group">
                    <select name="type_id" id="type_id" class="form-control">
                        <option value="all" @if($typeSelected=='all' ) selected='selected' @endif>Semua Tipe</option>
                        @foreach($types as $type)
                        <option value="{{ urlencode($type->name) }}" @if($typeSelected==urlencode($type->name))
                            selected='selected' @endif>
                            {{ $type->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-2 col-sm-6">
                <div class="form-group">
                    <select name="category_id" id="category_id" class="form-control">
                        <option value="all" @if($categorySelected=='all' ) selected='selected' @endif>Semua Kategori
                        </option>
                        @foreach($categories as $category)
                        <option value="{{ urlencode($category->name) }}" @if($categorySelected==urlencode($category->
                            name))
                            selected='selected' @endif>{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-2 col-sm-12">
                <div class="form-group">
                    <select name="tahun" id="tahun" class="form-control">
                        <option value="all" @if($tahunSelected=='all' ) selected='selected' @endif>Semua Tahun</option>
                        @for($tahun = \Carbon\Carbon::now()->year; $tahun >= 1900 ; $tahun--)
                        <option value="{{ $tahun }}" @if($tahunSelected==$tahun) selected='selected' @endif>{{ $tahun }}
                        </option>
                        @endfor
                    </select>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="form-group">
                    <input type="text" name="key" id="key" placeholder="Masukan judul atau keywords" class="form-control" value="{{ $key }}">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group text-right">
                    <button class="btn btn-info" id="btn-filter">Filter data</button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h5>Hasil pencarian</h5>
                <hr>
            </div>
        </div>
        <div class="row mb-5">
            @if($materials->isNotEmpty())
            @foreach($materials as $material)
            <div class="col-md-2 col-6 mb-3">
                <div class="card h-100">
                    <div class="embed-responsive embed-responsive-1by1">
                        <img class="card-img-top embed-responsive-item" src="{{ url('cover/' . $material->cover_path) }}" alt="{{ $material->title }}">
                    </div>
                    <div class="card-body">
                        <div class="d-flex no-block align-items-center m-b-5">
                            <!-- <span><i class="ti-calendar mr-2"></i>{{ \Carbon\Carbon::parse($material->year)->year }}</span> -->
                            <div class="ml-auto">
                                <!-- <a href="javascript:void(0)" class="link"><i class="ti-comments"></i> 3 Comments</a> -->
                            </div>
                        </div>
                        <!-- <h3 class="font-normal">{{ $material->title }}</h3> -->
                        <h5 class="text-truncate m-b-0 m-t-5">
                            @if(Auth()->user())
                            <a href="{{ url('dashboard/explore/' . $material->id . '/detail') }}" class="">{{ $material->title }}</a>
                            @else
                            <a href="{{ url('detail/' . $material->id) }}" class="">{{ $material->title }}</a>
                            @endif
                        </h5>
                    </div>
                </div>
            </div>
            @endforeach
            @else
            <div class="col-md-12" style="min-height: 50vh;">
                <h5 class="text-center">Tidak ditemukan</h5>
            </div>
            @endif
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                {{ $materials->links() }}
            </div>
        </div>
    </div>
</div>

@endsection
@push('js')
<script>
    $('#btn-filter').on('click', function() {
        var type = $('#type_id').val();
        var category = $('#category_id').val();
        var tahun = $('#tahun').val();
        var key = encodeURI($('#key').val());
        window.location.href = "{{ url('search/') }}" + '/' + type + '/' + category + '/' +
            tahun + '/' + key;
    })
</script>
@endpush