<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('adminbite/assets/images/favicon.png') }}">
    <title>Digital Library</title>
    <link href="{{ asset('adminbite/assets/libs/chartist/dist/chartist.min.css') }}" rel="stylesheet">
    <link href="{{ asset('adminbite/assets/extra-libs/c3/c3.min.css') }}" rel="stylesheet">
    <link href="{{ asset('adminbite/assets/libs/morris.js/morris.css') }}" rel="stylesheet">
    <link href="{{ asset('adminbite/css/style.min.css') }}" rel="stylesheet">
    @stack('css')
</head>

<body>
    <div class="main-wrapper">
        <div class="preloader">
            <div class="lds-ripple">
                <div class="lds-pos"></div>
                <div class="lds-pos"></div>
            </div>
        </div>
        <div class="auth-wrapper d-flex no-block justify-content-center align-items-center"
            style="background:url({{ asset('adminbite/assets/images/big/auth-bg.jpg') }}) no-repeat center center;">
            <div class="auth-box">
                @yield('content')
            </div>
        </div>
    </div>
    <script src="{{ asset('adminbite/assets/libs/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('adminbite/assets/libs/popper.js/dist/umd/popper.min.js') }}"></script>
    <script src="{{ asset('adminbite/assets/libs/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('adminbite/js/app.min.js') }}"></script>
    <!-- <script src="{{ asset('adminbite/js/app.init.light-sidebar.js') }}"></script> -->
    <script src="{{ asset('adminbite/js/app.init.dark.js') }}"></script>
    <script src="{{ asset('adminbite/js/app-style-switcher.js') }}"></script>
    <script src="{{ asset('adminbite/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js') }}"></script>
    <script src="{{ asset('adminbite/assets/extra-libs/sparkline/sparkline.js') }}"></script>
    <script src="{{ asset('adminbite/js/waves.js') }}"></script>
    <script src="{{ asset('adminbite/js/sidebarmenu.js') }}"></script>
    <script src="{{ asset('adminbite/js/custom.min.js') }}"></script>
    <script src="{{ asset('adminbite/assets/libs/chartist/dist/chartist.min.js') }}"></script>
    <script src="{{ asset('adminbite/assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js') }}">
    </script>
    <script src="{{ asset('adminbite/assets/extra-libs/c3/d3.min.js') }}"></script>
    <script src="{{ asset('adminbite/assets/extra-libs/c3/c3.min.js') }}"></script>
    <script src="{{ asset('adminbite/assets/libs/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('adminbite/assets/libs/morris.js/morris.min.js') }}"></script>

    <script src="{{ asset('adminbite/js/pages/dashboards/dashboard1.js') }}"></script>
    <script src="{{ asset('adminbite/assets/libs/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
    @stack('js')
</body>

</html>
