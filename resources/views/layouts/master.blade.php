<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('adminbite/assets/images/favicon.png') }}">
    <title>Digital Library</title>
    <link href="{{ asset('adminbite/assets/libs/chartist/dist/chartist.min.css') }}" rel="stylesheet">
    <link href="{{ asset('adminbite/assets/extra-libs/c3/c3.min.css') }}" rel="stylesheet">
    <link href="{{ asset('adminbite/assets/libs/morris.js/morris.css') }}" rel="stylesheet">
    <link href="{{ asset('adminbite/css/style.min.css') }}" rel="stylesheet">
    @stack('css')
</head>

<body>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper">
        @include('layouts.topbar')
        @include('layouts.sidebar')
        <div class="page-wrapper">
            @yield('bc')
            <div class="container-fluid">
                @yield('content')
            </div>
            <footer class="footer text-center">
                All Rights Reserved by Digital Library. Designed and Developed by
                <a href="#">Digital Library Team</a>.
            </footer>
        </div>
    </div>
    <script src="{{ asset('adminbite/assets/libs/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('adminbite/assets/libs/popper.js/dist/umd/popper.min.js') }}"></script>
    <script src="{{ asset('adminbite/assets/libs/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('adminbite/js/app.min.js') }}"></script>
    @if(Auth()->user()->role == 'admin' || Auth()->user()->role == 'operator')
    <script src="{{ asset('adminbite/js/app.init.dark.js') }}"></script>
    @elseif(Auth()->user()->role == 'student')
    <script src="{{ asset('adminbite/js/app.init.horizontal.js') }}"></script>
    @else
    <script src="{{ asset('adminbite/js/app.init.light-sidebar.js') }}"></script>
    @endif
    <script src="{{ asset('adminbite/js/app-style-switcher.js') }}"></script>
    <script src="{{ asset('adminbite/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js') }}"></script>
    <script src="{{ asset('adminbite/assets/extra-libs/sparkline/sparkline.js') }}"></script>
    <script src="{{ asset('adminbite/js/waves.js') }}"></script>
    <script src="{{ asset('adminbite/js/sidebarmenu.js') }}"></script>
    <script src="{{ asset('adminbite/js/custom.min.js') }}"></script>
    <script src="{{ asset('adminbite/assets/libs/chartist/dist/chartist.min.js') }}"></script>
    <script src="{{ asset('adminbite/assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js') }}">
    </script>
    <script src="{{ asset('adminbite/assets/extra-libs/c3/d3.min.js') }}"></script>
    <script src="{{ asset('adminbite/assets/extra-libs/c3/c3.min.js') }}"></script>
    <script src="{{ asset('adminbite/assets/libs/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('adminbite/assets/libs/morris.js/morris.min.js') }}"></script>

    <script src="{{ asset('adminbite/js/pages/dashboards/dashboard1.js') }}"></script>
    <script src="{{ asset('adminbite/assets/libs/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
    @if(Session::get('icon'))
    <script>
        swal({
            title: "{{ Session::get('title') }}",
            text: "{{ Session::get('text') }}",
            icon: "{{ Session::get('icon') }}",
        })
    </script>
    @endif
    @stack('js')
</body>

</html>