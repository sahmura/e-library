<aside class="left-sidebar">
    <div class="scroll-sidebar">
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                @if(Auth()->user()->role == 'admin' || Auth()->user()->role == 'operator')
                <li>
                    <div class="user-profile dropdown m-t-20">
                        <div class="user-pic">
                            <img src="{{ asset('adminbite/assets/images/users/profile.svg') }}" alt="users" class="rounded-circle img-fluid" />
                        </div>
                        <div class="user-content hide-menu m-t-10">
                            <h5 class="m-b-10 user-name font-medium">{{ Auth()->user()->name }}</h5>
                        </div>
                    </div>
                </li>
                <li class="nav-small-cap">
                    <i class="mdi mdi-dots-horizontal"></i>
                    @if(Auth()->user()->role == 'admin')
                    <span class="hide-menu">Admin</span>
                    @elseif(Auth()->user()->role == 'operator')
                    <span class="hide-menu">Operator</span>
                    @endif
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark" href="{{ url('admin') }}" aria-expanded="false">
                        <i class="mdi mdi-home"></i>
                        <span class="hide-menu">Dashboards </span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark" href="{{ url('admin/katalog') }}" aria-expanded="false">
                        <i class="mdi mdi-book-multiple-variant"></i>
                        <span class="hide-menu">Katalog </span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark" href="{{ url('admin/statistik') }}" aria-expanded="false">
                        <i class="mdi mdi-chart-areaspline"></i>
                        <span class="hide-menu">Statistic </span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark" href="{{ url('admin/author') }}" aria-expanded="false">
                        <i class="mdi mdi-account-edit"></i>
                        <span class="hide-menu">Author </span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark" href="{{ url('admin/peminjaman') }}" aria-expanded="false">
                        <i class="mdi mdi-share-variant"></i>
                        <span class="hide-menu">Peminjaman </span>
                    </a>
                </li>
                @if(Auth()->user()->role == 'admin')
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark" href="{{ url('admin/user') }}" aria-expanded="false">
                        <i class="mdi mdi-account-multiple"></i>
                        <span class="hide-menu">Users </span>
                    </a>
                </li>
                @endif
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark" href="{{ url('admin/category') }}" aria-expanded="false">
                        <i class="mdi mdi-format-list-bulleted"></i>
                        <span class="hide-menu">Kelola Kategori </span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark" href="{{ url('admin/type') }}" aria-expanded="false">
                        <i class="mdi mdi-library-books"></i>
                        <span class="hide-menu">Kelola Tipe </span>
                    </a>
                </li>
                @elseif(Auth()->user()->role == 'student')
                @if(Auth()->user()->prodi_id != 0 || Auth()->user()->prodi_id != null)
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark" href="{{ url('dashboard') }}" aria-expanded="false">
                        <i class="mdi mdi-home"></i>
                        <span class="hide-menu">Dashboards </span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark" href="{{ url('dashboard/explore') }}" aria-expanded="false">
                        <i class="mdi mdi-compass"></i>
                        <span class="hide-menu">Telusuri </span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark" href="{{ url('dashboard/my') }}" aria-expanded="false">
                        <i class="mdi mdi-book-multiple"></i>
                        <span class="hide-menu">Kelola Pinjaman </span>
                    </a>
                </li>
                <!-- <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark" href="{{ url('dashboard/calendar') }}"
                        aria-expanded="false">
                        <i class="mdi mdi-calendar-clock"></i>
                        <span class="hide-menu">Kalendar </span>
                    </a>
                </li> -->
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark" href="{{ url('dashboard/setting') }}" aria-expanded="false">
                        <i class="mdi mdi-account-settings-variant"></i>
                        <span class="hide-menu">Pengaturan </span>
                    </a>
                </li>
                @endif
                @endif
            </ul>
        </nav>
    </div>
</aside>