<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('adminbite/landing/assets/images/favicon.png') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Digital Library</title>
    <link href="{{ asset('adminbite/landing/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('adminbite/landing/assets/owl.carousel/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('adminbite/landing/assets/owl.carousel/owl.theme.default.css') }}" rel="stylesheet">
    <link href="{{ asset('adminbite/landing/css/style.css') }}" rel="stylesheet">
    @stack('css')
</head>

<body class="fix-header">
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <div id="main-wrapper">
        <header class="topheader" id="top">
            <div class="fix-width">
                <nav class="navbar navbar-expand-md navbar-light p-l-0">
                    <a class="navbar-brand" href="{{ url('')}}">
                        <img src="{{ asset('adminbite/assets/images/unnes.png') }}" width="25px" alt="Logo" class="mr-1" />
                        Digital Library
                    </a>
                    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    @if(Auth()->user())
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul class="navbar-nav ml-auto stylish-nav">
                            <li class="nav-item">
                                <a class="m-t-5 btn btn-danger font-13" href="{{ url('dashboard') }}" style="width:120px;">Dashboard</a>
                            </li>
                        </ul>
                    </div>
                    @else
                    <div class="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul class="navbar-nav ml-auto stylish-nav">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('search') }}">Cari</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">Masuk</a>
                            </li>
                            <li class="nav-item">
                                <a class="m-t-5 btn btn-danger font-13" href="{{ route('register') }}" style="width:120px;">Daftar</a>
                            </li>
                        </ul>
                    </div>
                    @endif
                </nav>
            </div>
        </header>
        <div class="page-wrapper">
            <div class="container-fluid">
                @yield('content')
                <a class="bt-top btn btn-circle btn-lg btn-info" href="#top">
                    <i class="ti-arrow-up"></i>
                </a>
                <footer class="footer row">
                    <div class="fix-width">
                        <div class="row d-flex justify-content-center">
                            <div class="col-md-6">
                                <!-- <img src="{{ url('adminbite/assets/images/unnes.png') }}" alt="Logo Unnes" class="img-fluid" width="80px" /> -->
                                <p class="m-t-30 text-center" style="font-size: 1.1em;">
                                    <span class="text-white">Digital Library Pascasarjana Universitas Negeri Semarang<br>
                                    </span> Gedung A Kampus Pascasarjana Jl Kelud Utara III, Semarang 50237. Telp. 62248440516,. WhatsApp Telegram 085742728359. </p>
                            </div>
                        </div>
                        <div class="col-md-12 sub-footer">
                            <span>Copyright 2019. All Rights Reserved by
                                <a class="text-white" href="#" target="_blank">Digital Library
                                </a>
                            </span>
                            <span class="pull-right">Design & Developed by
                                <a class="text-white" href="#" target="_blank">Tim Digital
                                    Library</a>
                            </span>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </div>
    <script src="{{ asset('adminbite/landing/js/jquery.min.js') }}"></script>
    <script src="{{ asset('adminbite/landing/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ asset('adminbite/landing/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('adminbite/landing/js/waves.js') }}"></script>
    <script src="{{ asset('adminbite/landing/js/custom.min.js') }}"></script>
    @stack('js')
</body>

</html>