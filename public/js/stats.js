$('#getTotalPinjamByYear-button').on('click', function () {
    $.ajax({
        url: "{{ url('statistik/getTotalPinjamByYear') }}",
        method: 'GET',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            var label = data.label;
            var value = data.value;
            var type = data.type;
            var color = data.color;
            var ctx = document.getElementById('getTotalPinjamByYear').getContext('2d');
            var getTotalPinjamByYear = new Chart(ctx, {
                type: type,
                data: {
                    labels: label,
                    datasets: [{
                        data: value,
                        label: 'Total Pinjaman',
                        backgroundColor: color[0],
                    }]
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'bottom'
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                min: 0, // it is for ignoring negative step.
                                beginAtZero: true,
                                stepSize: 1
                            }
                        }]
                    },
                }
            })
        }
    });
});

$('#getTotalUnduhByYear-button').on('click', function () {
    $.ajax({
        url: "{{ url('statistik/getTotalUnduhByYear') }}",
        method: 'GET',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            var label = data.label;
            var value = data.value;
            var type = data.type;
            var color = data.color;
            var ctx = document.getElementById('getTotalUnduhByYear').getContext('2d');
            var getTotalUnduhByYear = new Chart(ctx, {
                type: type,
                data: {
                    labels: label,
                    datasets: [{
                        data: value,
                        label: 'Total Unduhan',
                        backgroundColor: color[0],
                    }]
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'bottom'
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                min: 0, // it is for ignoring negative step.
                                beginAtZero: true,
                                stepSize: 1
                            }
                        }]
                    },
                }
            })
        }
    });
});

$('#getTotalUserByProdiS-button').on('click', function () {
    $.ajax({
        url: "{{ url('statistik/getTotalUserByProdiS') }}",
        method: 'GET',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            var label = data.label;
            var value = data.value;
            var type = data.type;
            var color = data.color;
            var ctx = document.getElementById('getTotalUserByProdiS').getContext('2d');
            var getTotalUserByProdiS = new Chart(ctx, {
                type: type,
                data: {
                    labels: label,
                    datasets: [{
                        data: value,
                        label: 'Total Pengguna Jenjang S1',
                        backgroundColor: color,
                    }]
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'bottom',
                        display: false
                    }
                }
            })
        }
    });
});

$('#getTotalUserByProdiM-button').on('click', function () {
    $.ajax({
        url: "{{ url('statistik/getTotalUserByProdiM') }}",
        method: 'GET',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            var label = data.label;
            var value = data.value;
            var type = data.type;
            var color = data.color;
            var ctx = document.getElementById('getTotalUserByProdiM').getContext('2d');
            new Chart(ctx, {
                type: type,
                data: {
                    datasets: [{
                        data: value,
                        backgroundColor: color,
                        label: 'Total Pengguna Jenjang S2'
                    }],
                    labels: label
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'bottom',
                        display: false,
                    }
                }
            })
        }
    });
});

$('#getTotalByYear-button').on('click', function () {
    $.ajax({
        url: "{{ url('statistik/getTotalByYear') }}",
        method: 'GET',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            var label = data.label;
            var value = data.value;
            var type = data.type;
            var color = data.color;
            var ctx = document.getElementById('getTotalByYear').getContext('2d');
            new Chart(ctx, {
                type: type,
                data: {
                    datasets: [{
                        data: value,
                        backgroundColor: color,
                        label: 'Total Per Tahun Terbit'
                    }],
                    labels: label
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'bottom',
                        display: false,
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                min: 0, // it is for ignoring negative step.
                                beginAtZero: true,
                                stepSize: 1
                            }
                        }]
                    },
                }
            })
        }
    });
});

$('#getTotalByType-button').on('click', function () {
    $.ajax({
        url: "{{ url('statistik/getTotalByType') }}",
        method: 'GET',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            var label = data.label;
            var value = data.value;
            var type = data.type;
            var color = data.color;
            var ctx = document.getElementById('getTotalByType').getContext('2d');
            new Chart(ctx, {
                type: type,
                data: {
                    datasets: [{
                        data: value,
                        backgroundColor: color,
                        label: 'Total Per Tipe'
                    }],
                    labels: label
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'bottom',
                        display: false,
                    }
                }
            })
        }
    });
});

$('#getTotalByCategory-button').on('click', function () {
    $.ajax({
        url: "{{ url('statistik/getTotalByCategory') }}",
        method: 'GET',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            var label = data.label;
            var value = data.value;
            var type = data.type;
            var color = data.color;
            var ctx = document.getElementById('getTotalByCategory').getContext('2d');
            new Chart(ctx, {
                type: type,
                data: {
                    datasets: [{
                        data: value,
                        backgroundColor: color,
                        label: 'Total Per Kategori'
                    }],
                    labels: label
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'bottom',
                        display: false,
                    }
                }
            })
        }
    });
});
