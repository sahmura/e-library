<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuthorMaterial extends Model
{
    protected $table = 'author_material';
    protected $fillable = [
        'author_id',
        'material_id'
    ];
}
