<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Material extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'title',
        'abstract',
        'description',
        'category_id',
        'type_id',
        'cover_path',
        'isbn',
        'volume',
        'edition',
        'number',
        'publisher',
        'year',
        'keywords',
        'download_path',
        'downloads',
        'reads',
    ];

    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id', 'id');
    }

    public function type()
    {
        return $this->belongsTo('App\Type', 'type_id', 'id');
    }

    public function authors()
    {
        return $this->belongsToMany('App\Author');
    }
}
