<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prodi extends Model
{
    protected $fillable = [
        'faculty_id',
        'name',
        'code',
        'jenjang'
    ];

    public function faculty()
    {
        return $this->belongsTo('App\Faculty', 'faculty_id', 'id');
    }

    public function users()
    {
        return $this->hasMany('App\User', 'prodi_id', 'id');
    }
}
