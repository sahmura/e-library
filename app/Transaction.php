<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Transaction extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'user_id',
        'material_id',
        'due_date',
        'type',
        'status'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function material()
    {
        return $this->belongsTo('App\Material', 'material_id', 'id');
    }

    public function getSisaWaktuAttribute()
    {
        $due_date = Carbon::parse($this->due_date);
        return $due_date->diffInDays(Carbon::now());
    }
}
