<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Author extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'name',
        'email',
        'institution',
        'department',
        'country_id',
    ];

    public function country()
    {
        return $this->belongsTo('App\Country', 'country_id', 'id');
    }

    public function materials()
    {
        return $this->belongsToMany('App\Material');
    }
}
