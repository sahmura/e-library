<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Material;
use App\Transaction;
use App\User;
use App\Category;
use App\Prodi;
use App\Type;
use DB;

class StatisticController extends Controller
{
    public function index()
    {
        return view('statistic');
    }
    public function indexAdmin()
    {
        return view('admin.statistic.index_statistic');
    }
    public function getTotalByCategory()
    {
        $data = Category::select('name')->withCount('materials')->get()->toArray();

        $labels = [];
        $values = [];
        $colors = [];
        for ($indexLabel = 0; $indexLabel < count($data); $indexLabel++) {
            $labels[] = $data[$indexLabel]['name'];
            $values[] = $data[$indexLabel]['materials_count'];
            $colors[] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
        }

        $result = [
            'label' => $labels,
            'value' => $values,
            'color' => $colors,
            'type' => 'pie',
        ];
        return response()->json($result);
    }

    public function getTotalByType()
    {
        $data = Type::select('name')->withCount('materials')->get()->toArray();

        $labels = [];
        $values = [];
        $colors = [];
        for ($indexLabel = 0; $indexLabel < count($data); $indexLabel++) {
            $labels[] = $data[$indexLabel]['name'];
            $values[] = $data[$indexLabel]['materials_count'];
            $colors[] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
        }

        $result = [
            'label' => $labels,
            'value' => $values,
            'color' => $colors,
            'type' => 'pie',
        ];
        return response()->json($result);
    }

    public function getTotalByYear()
    {
        $data = Material::select(DB::raw('count(*) as value , YEAR(year) as label'))
            ->where('deleted_at', null)
            ->groupBy(DB::raw("YEAR(year)"))
            ->get()->toArray();

        $labels = [];
        $values = [];
        $colors = [];
        for ($indexLabel = 0; $indexLabel < count($data); $indexLabel++) {
            $labels[] = $data[$indexLabel]['label'];
            $values[] = $data[$indexLabel]['value'];
            $colors[] = $colors[] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
        }

        $result = [
            'label' => $labels,
            'value' => $values,
            'color' => $colors,
            'type' => 'bar',
        ];
        return response()->json($result);
    }

    public function getTotalUserByProdiS()
    {
        $data = Prodi::select('name')->where('jenjang', 'S3')
            ->withCount('users')->get()->toArray();

        $labels = [];
        $values = [];
        $colors = [];
        for ($indexLabel = 0; $indexLabel < count($data); $indexLabel++) {
            $labels[] = $data[$indexLabel]['name'];
            $values[] = $data[$indexLabel]['users_count'];
            $colors[] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
        }

        $result = [
            'label' => $labels,
            'value' => $values,
            'color' => $colors,
            'type' => 'pie',
        ];
        return response()->json($result);
    }

    public function getTotalUserByProdiM()
    {
        $data = Prodi::select('name')->where('jenjang', 'S2')
            ->withCount('users')->get()->toArray();

        $labels = [];
        $values = [];
        $colors = [];
        for ($indexLabel = 0; $indexLabel < count($data); $indexLabel++) {
            $labels[] = $data[$indexLabel]['name'];
            $values[] = $data[$indexLabel]['users_count'];
            $colors[] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
        }

        $result = [
            'label' => $labels,
            'value' => $values,
            'color' => $colors,
            'type' => 'pie',
        ];
        return response()->json($result);
    }

    public function getTotalPinjamByYear()
    {
        $data = Transaction::select(DB::raw('count(*) as value, YEAR(created_at) as label'))
            ->where('type', 'PINJAM')
            ->where('status', 'SUCCESS')
            ->groupBy(DB::raw('YEAR(created_at)'))->latest()->get()->toArray();

        $labels = [];
        $values = [];
        $colors = [];
        for ($indexLabel = 0; $indexLabel < count($data); $indexLabel++) {
            $labels[] = $data[$indexLabel]['label'];
            $values[] = $data[$indexLabel]['value'];
            $colors[] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
        }

        $result = [
            'label' => $labels,
            'value' => $values,
            'color' => $colors,
            'type' => 'bar',
        ];
        return response()->json($result);
    }

    public function getTotalUnduhByYear()
    {
        $data = Transaction::select(DB::raw('count(*) as value, YEAR(created_at) as label'))
            ->where('type', 'UNDUH')
            ->where('status', 'SUCCESS')
            ->groupBy(DB::raw('YEAR(created_at)'))->latest()->get()->toArray();

        $labels = [];
        $values = [];
        $colors = [];
        for ($indexLabel = 0; $indexLabel < count($data); $indexLabel++) {
            $labels[] = $data[$indexLabel]['label'];
            $values[] = $data[$indexLabel]['value'];
            $colors[] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
        }

        $result = [
            'label' => $labels,
            'value' => $values,
            'color' => $colors,
            'type' => 'bar',
        ];
        return response()->json($result);
    }

    public function generateOutput($data, $chartType, $nameTag = 'label', $valueTag = 'value')
    {
        $labels = [];
        $values = [];
        for ($indexLabel = 0; $indexLabel < count($data); $indexLabel++) {
            $labels[] = $data[$indexLabel][$nameTag];
            $values[] = $data[$indexLabel][$valueTag];
        }
        $label = implode(',', $labels);
        $value = implode(',', $values);
        $result = [
            'label' => $label,
            'value' => $value
        ];
        return response()->json($result);
        // dd($result);
    }
}
