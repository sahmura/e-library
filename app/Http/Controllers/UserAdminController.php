<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Yajra\DataTables\Facades\DataTables;
use DB;

class UserAdminController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = User::latest()->get();
            return DataTables::of($data)
                ->addColumn(
                    'department',
                    function ($data) {
                        return ($data->prodi_id <> 0) ? $data->prodi_id : 'Belum disetting';
                    }
                )
                ->addColumn(
                    'role',
                    function ($data) {
                        return ucfirst($data->role);
                    }
                )
                ->addColumn(
                    'action',
                    function ($data) {
                        return '<div class="btn btn-group">
                            <button class="btn btn-info btn-detail btn-xs"
                                data-id="' . $data->id . '"
                                data-name="' . $data->name . '"
                                data-email="' . $data->email . '"
                                data-role="' . $data->role . '"
                            >
                                <i class="mdi mdi-account"></i>
                            </button>
                        </div>';
                    }
                )
                ->addIndexColumn()
                ->make(true);
        }
        return view('admin.user.adminuser');
    }

    public function changeRole(Request $req)
    {
        try {
            DB::beginTransaction();
            User::where('id', $req->id)->update(
                [
                    'role' => $req->role
                ]
            );
            DB::commit();
            $res = [
                'status' => true,
                'title' => 'Berhasil Merubah Hak Akses',
                'text' => 'Semua fitur di dalam hak akses tersebut dapat diakses',
                'icon' => 'success'
            ];
        } catch (\Exception $e) {
            throw $e;
            DB::rollback();
            $res = [
                'status' => false,
                'title' => 'Gagal Merubah Hak Akses',
                'text' => $e->getMessage(),
                'icon' => 'error'
            ];
        }

        return response()->json($res);
    }
}
