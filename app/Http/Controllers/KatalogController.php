<?php

namespace App\Http\Controllers;

use App\Author;
use App\AuthorMaterial;
use App\Category;
use App\Material;
use App\Type;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use DB;
use Yajra\DataTables\Facades\DataTables;

class KatalogController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Material::where('deleted_at', null)->with(['authors', 'category'])->latest()->get();
            return Datatables::of($data)
                ->addColumn(
                    'isbn',
                    function ($data) {
                        return ($data->isbn <> '') ? $data->isbn : 'Tidak berkategori';
                    }
                )
                ->addColumn(
                    'category',
                    function ($data) {
                        return (isset($data->category->name)) ? $data->category->name : 'Tidak berkategori';
                    }
                )
                ->addColumn(
                    'action',
                    function ($data) {
                        return '<div class="btn-group">
                            <button class="btn btn-info btn-sm btn-detail"
                            data-id="' . $data->id . '">
                                <i class="mdi mdi-play"></i>
                            </button>
                            <button class="btn btn-warning btn-sm btn-edit"
                            data-id="' . $data->id . '">
                                <i class="mdi mdi-pencil"></i>
                            </button>
                            <button class="btn btn-danger btn-sm btn-delete"
                            data-id="' . $data->id . '">
                                <i class="mdi mdi-delete"></i>
                            </button>
                        </div>';
                    }
                )
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.katalog.adminkatalog');
    }

    public function createPage()
    {
        $categories = Category::where('deleted_at', null)->get();
        $types = Type::where('deleted_at', null)->get();
        $authors = Author::where('deleted_at', null)->get();
        return view('admin.katalog.katalogcreate', compact('categories', 'types', 'authors'));
    }

    public function editPage($id)
    {
        $data = Material::where('id', $id)->where('deleted_at', null)->with('authors')->first();
        $authors = Author::where('deleted_at', null)->get();
        $categories = Category::where('deleted_at', null)->get();
        $types = Type::where('deleted_at', null)->get();
        return view('admin.katalog.katalogedit', compact('data', 'authors', 'categories', 'types'));
    }

    public function detailPage($id)
    {
        $data = Material::where('id', $id)->where('deleted_at', null)->with('authors')->first();
        return view('admin.katalog.katalogdetail', compact('data'));
    }

    public function addData(Request $request)
    {
        try {
            DB::beginTransaction();
            $validate = $request->validate(
                [
                    'cover' => 'required|image|mimes:jpeg,png,jpg|max:2048',
                    'file' => 'required|file|mimes:pdf,epub,jpg,png,jpeg',
                ]
            );
            $cover_path = md5(str_replace(' ', '-', $request->title) . '-' . Carbon::now()->format('d-m-Y')) . '-logo.' . $request->cover->extension();
            $download_path = md5(str_replace(' ', '-', $request->title) . '-' . Carbon::now()->format('d-m-Y')) . '.' . $request->file->extension();
            $request->cover->move('cover', $cover_path);
            $request->file->move('file', $download_path);

            $material = Material::create(
                [
                    'title' => $request->title,
                    'abstract' => $request->abstract ?? '',
                    'description' => $request->description,
                    'category_id' => $request->category_id,
                    'type_id' => $request->type_id,
                    'cover_path' => $cover_path,
                    'isbn' => $request->isbn ?? '',
                    'volume' => $request->volume ?? '',
                    'edition' => $request->edition ?? '',
                    'number' => $request->number ?? '',
                    'publisher' => $request->publisher ?? '',
                    'year' => Carbon::parse($request->year)->format('Y-m-d'),
                    'keywords' => $request->keywords,
                    'download_path' => $download_path,
                    'downloads' => 0,
                    'reads' => 0,
                ]
            );

            for ($index = 0; $index < count($request->author_id); $index++) {
                AuthorMaterial::create(
                    [
                        'author_id' => $request->author_id[$index],
                        'material_id' => $material->id
                    ]
                );
            }

            $response = [
                'status' => true,
                'title' => 'Berhasil menambahkan data',
                'text' => '',
                'icon' => 'success'
            ];
            DB::commit();
            $request->session()->flash('title', $response['title']);
            $request->session()->flash('text', $response['text']);
            $request->session()->flash('icon', $response['icon']);
            return redirect('admin/katalog');
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
            $response = [
                'status' => false,
                'title' => 'Gagal menambahkan data',
                'text' => $e->getMessage(),
                'icon' => 'error'
            ];
            $request->session()->flash('title', $response['title']);
            $request->session()->flash('text', $response['text']);
            $request->session()->flash('icon', $response['icon']);
            return redirect('admin/katalog/create');
        }
    }

    public function editData(Request $request)
    {
        try {
            DB::beginTransaction();
            // $validate = $request->validate(
            //     [
            //         'cover' => 'image|mimes:jpeg,png,jpg|max:2048',
            //         'file' => 'file|mimes:pdf,docx,epub',
            //     ]
            // );
            $oldData = Material::where('id', $request->id)->first();

            if (isset($request->cover)) {
                $cover_path = md5(str_replace(' ', '-', $request->title) . '-' . Carbon::now()->format('d-m-Y')) . '-logo.' . $request->cover->extension();
                $request->cover->move('cover', $cover_path);
                Storage::delete('cover', $oldData->cover_path);
            } else {
                $cover_path = $oldData->cover_path;
            }

            if (isset($request->file)) {
                $download_path = md5(str_replace(' ', '-', $request->title) . '-' . Carbon::now()->format('d-m-Y')) . '.' . $request->file->extension();
                $request->file->move('file', $download_path);
                Storage::delete('file', $oldData->download_path);
            } else {
                $download_path = $oldData->download_path;
            }

            AuthorMaterial::where('material_id', $request->id)->delete();
            Material::where('id', $request->id)->update(
                [
                    'title' => $request->title,
                    'abstract' => $request->abstract ?? '',
                    'description' => $request->description,
                    'category_id' => $request->category_id,
                    'type_id' => $request->type_id,
                    'cover_path' => $cover_path,
                    'isbn' => $request->isbn ?? '',
                    'volume' => $request->volume ?? '',
                    'edition' => $request->edition ?? '',
                    'number' => $request->number ?? '',
                    'publisher' => $request->publisher ?? '',
                    'year' => Carbon::parse($request->year)->format('Y-m-d'),
                    'keywords' => $request->keywords,
                    'download_path' => $download_path,
                ]
            );

            for ($index = 0; $index < count($request->author_id); $index++) {
                AuthorMaterial::create(
                    [
                        'author_id' => $request->author_id[$index],
                        'material_id' => $request->id
                    ]
                );
            }

            $response = [
                'status' => true,
                'title' => 'Berhasil menyunting data',
                'text' => '',
                'icon' => 'success'
            ];
            DB::commit();
            $request->session()->flash('title', $response['title']);
            $request->session()->flash('text', $response['text']);
            $request->session()->flash('icon', $response['icon']);
            return redirect('admin/katalog');
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
            $response = [
                'status' => false,
                'title' => 'Gagal menyunting data',
                'text' => $e->getMessage(),
                'icon' => 'error'
            ];
            $request->session()->flash('title', $response['title']);
            $request->session()->flash('text', $response['text']);
            $request->session()->flash('icon', $response['icon']);
            return redirect('admin/katalog/' . $request->id . '/edit');
        }
    }

    public function deleteData(Request $request)
    {
        try {
            DB::beginTransaction();
            Material::where('id', $request->id)->delete();
            $response = [
                'status' => true,
                'title' => 'Berhasil menghapus data',
                'text' => '',
                'icon' => 'success'
            ];
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
            $response = [
                'status' => false,
                'title' => 'Gagal menyunting data',
                'text' => $e->getMessage(),
                'icon' => 'error'
            ];
        }

        return response()->json($response);
    }
}
