<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Type;
use App\Category;
use App\Material;
use App\Transaction;
use Carbon\Carbon;

class ExploreController extends Controller
{
    public function index()
    {
        $categories = Category::orderBy('name', 'ASC')->get();
        $types = Type::orderBy('name', 'ASC')->get();
        $materials = Material::where('deleted_at', null)->latest()->paginate(12);
        return view('user.explore.indexexplore', compact('categories', 'types', 'materials'));
    }

    public function detail($id)
    {
        $material = Material::where('deleted_at', null)->where('id', $id)->with('category')->first();
        $totalPinjam = Transaction::where('material_id', $material->id)->where('type', 'PINJAM')->count();
        $isBorrowed = Transaction::where('material_id', $material->id)->where('user_id', Auth()->user()->id)->where('type', 'PINJAM')->whereDate('due_date', '>=', Carbon::now())->count();
        return view('user.explore.detailexplore', compact('material', 'totalPinjam', 'isBorrowed'));
    }

    public function searchBy($type = 'all', $category = 'all', $tahun = 'all', $key = 'all')
    {
        if ($type == 'all' && $category == 'all' && $tahun == 'all' && $key = 'all') {
            $materials = Material::where('deleted_at', null)->paginate(12);
        } else {
            $typeQuery = (urldecode($type) <> 'all') ? Type::where('name', 'LIKE', '%' . urldecode($type) . '%')->first()->id : '';
            $categoryQuery = (urldecode($category) <> 'all') ? Category::where('name', 'LIKE', '%' . urldecode($category) . '%')->first()->id : '';
            $tahunQuery = (urldecode($tahun) <> 'all') ? $tahun : '';
            $key = urldecode($key);
            $materials = Material::where('type_id', 'LIKE', '%' . $typeQuery . '%')
                ->where('category_id', 'LIKE', '%' . $categoryQuery . '%')
                ->where('year', 'LIKE', '%' . $tahunQuery . '%')
                ->orWhere('title', 'LIKE', '%' . $key . '%')
                ->orWhere('keywords', 'LIKE', '%' . $key . '%')
                ->paginate(12);
        }
        // dd($typeQuery . $categoryQuery . $tahunQuery);
        $typeSelected = $type;
        $categorySelected = $category;
        $tahunSelected = $tahun;
        $categories = Category::orderBy('name', 'ASC')->get();
        $types = Type::orderBy('name', 'ASC')->get();
        return view('user.explore.searchresult', compact('categories', 'types', 'materials', 'typeSelected', 'categorySelected', 'tahunSelected', 'key'));
    }
}
