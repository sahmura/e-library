<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Author;
use App\Country;
use Yajra\DataTables\Facades\DataTables;

class AuthorController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Author::where('deleted_at', null)->with('country')->orderBy('name', 'ASC')->get();
            return DataTables::of($data)
                ->addColumn(
                    'country',
                    function ($data) {
                        return $data->country->name;
                    }
                )
                ->addColumn(
                    'action',
                    function ($data) {
                        return '<div class="btn-group">
                            <button class="btn btn-warning btn-sm btn-edit"
                            data-id="' . $data->id . '"
                            data-name="' . $data->name . '"
                            data-email="' . $data->email . '"
                            data-institution="' . $data->institution . '"
                            data-department="' . $data->department . '"
                            data-country="' . $data->country_id . '">
                                <i class="mdi mdi-pencil"></i>
                            </button>
                            <button class="btn btn-danger btn-sm btn-delete"
                            data-id="' . $data->id . '">
                                <i class="mdi mdi-delete"></i>
                            </button>
                        </div>';
                    }
                )
                ->addIndexColumn()
                ->make(true);
        }

        $countries = Country::get();
        return view('admin.author.adminauthor', compact('countries'));
    }

    public function addData(Request $request)
    {
        try {
            Author::create(
                [
                    'name' => $request->name,
                    'email' => $request->email,
                    'institution' => $request->institution,
                    'department' => $request->department,
                    'country_id' => $request->country_id,
                ]
            );
            $response = [
                'status' => true,
                'title' => 'Berhasil menambahkan data',
                'text' => '',
                'type' => 'success'
            ];
        } catch (\Exception $e) {
            throw $e;
            $response = [
                'status' => false,
                'title' => 'Gagal menambahkan data',
                'text' => $e->getMessage(),
                'type' => 'error'
            ];
        }

        return response()->json($response);
    }

    public function editData(Request $request)
    {
        try {
            Author::where('id', $request->id)->update(
                [
                    'name' => $request->name,
                    'email' => $request->email,
                    'institution' => $request->institution,
                    'department' => $request->department,
                    'country_id' => $request->country_id,
                ]
            );
            $response = [
                'status' => true,
                'title' => 'Berhasil menyunting data',
                'text' => '',
                'type' => 'success'
            ];
        } catch (\Exception $e) {
            throw $e;
            $response = [
                'status' => false,
                'title' => 'Gagal menyunting data',
                'text' => $e->getMessage(),
                'type' => 'error'
            ];
        }

        return response()->json($response);
    }

    public function deleteData(Request $request)
    {
        try {
            Author::where('id', $request->id)->delete();
            $response = [
                'status' => true,
                'title' => 'Berhasil menghapus data',
                'text' => '',
                'type' => 'success'
            ];
        } catch (\Exception $e) {
            throw $e;
            $response = [
                'status' => false,
                'title' => 'Gagal menghapus data',
                'text' => $e->getMessage(),
                'type' => 'error'
            ];
        }

        return response()->json($response);
    }

    public function getAuthor(Request $request)
    {
        $data = Author::where('name', 'LIKE', '%' . $request->term . '%')
            ->orWhere('email', 'LIKE', '%' . $request->term . '%')
            ->get();

        $result = [];
        foreach ($data as $res) {
            $result[] = [
                'id' => $res->id,
                'text' => $res->name . ' (' . $res->email . ')'
            ];
        }

        return response()->json($result);
    }
}
