<?php

namespace App\Http\Controllers;

use App\Category;
use App\Material;
use App\Type;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function search($type = 'all', $category = 'all', $tahun = 'all', $key = 'all')
    {
        if ($type == 'all' && $category == 'all' && $tahun == 'all' && $key = 'all') {
            $materials = Material::where('deleted_at', null)->paginate(12);
        } else {
            $typeQuery = (urldecode($type) <> 'all') ? Type::where('name', 'LIKE', '%' . urldecode($type) . '%')->first()->id : '';
            $categoryQuery = (urldecode($category) <> 'all') ? Category::where('name', 'LIKE', '%' . urldecode($category) . '%')->first()->id : '';
            $tahunQuery = (urldecode($tahun) <> 'all') ? $tahun : '';
            $key = urldecode($key);
            $materials = Material::where('type_id', 'LIKE', '%' . $typeQuery . '%')
                ->where('category_id', 'LIKE', '%' . $categoryQuery . '%')
                ->where('year', 'LIKE', '%' . $tahunQuery . '%')
                ->orWhere('title', 'LIKE', '%' . $key . '%')
                ->orWhere('keywords', 'LIKE', '%' . $key . '%')
                ->paginate(12);
        }
        // dd($typeQuery . $categoryQuery . $tahunQuery);
        $typeSelected = $type;
        $categorySelected = $category;
        $tahunSelected = $tahun;
        $categories = Category::orderBy('name', 'ASC')->get();
        $types = Type::orderBy('name', 'ASC')->get();
        return view('search', compact('categories', 'types', 'materials', 'typeSelected', 'categorySelected', 'tahunSelected', 'key'));
    }

    public function detail($id)
    {
        $material = Material::where('deleted_at', null)->where('id', $id)->with('category')->first();
        return view('detail_result', compact('material'));
    }
}
