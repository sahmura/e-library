<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Type;
use App\Material;
use Yajra\DataTables\Facades\DataTables;

class TypeController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Type::where('deleted_at', null)->orderBy('name', 'ASC')->get();
            return DataTables::of($data)
                ->addColumn(
                    'action',
                    function ($data) {
                        return '<div class="btn-group">
                            <button class="btn btn-warning btn-sm btn-edit"
                            data-id="' . $data->id . '"
                            data-name="' . $data->name . '"
                            data-description="' . $data->description . '">
                                <i class="mdi mdi-pencil"></i>
                            </button>
                            <button class="btn btn-danger btn-sm btn-delete"
                            data-id="' . $data->id . '">
                                <i class="mdi mdi-delete"></i>
                            </button>
                        </div>';
                    }
                )
                ->addIndexColumn()
                ->make(true);
        }

        return view('admin.type.admintype');
    }

    public function addData(Request $request)
    {
        try {
            if (Type::where('name', $request->name)->where('deleted_at', null)->count() <> 0) {
                $response = [
                    'status' => false,
                    'title' => 'Data sudah tersedia',
                    'text' => '',
                    'type' => 'error'
                ];

                return response()->json($response);
            }
            Type::create(
                [
                    'name' => $request->name,
                    'description' => $request->description
                ]
            );
            $response = [
                'status' => true,
                'title' => 'Berhasil menambahkan data',
                'text' => '',
                'type' => 'success'
            ];
        } catch (\Exception $e) {
            throw $e;
            $response = [
                'status' => false,
                'title' => 'Gagal menambahkan data',
                'text' => $e->getMessage(),
                'type' => 'error'
            ];
        }

        return response()->json($response);
    }

    public function editData(Request $request)
    {
        try {
            if (Type::where('name', $request->name)->whereNotIn('id', [$request->id])->where('deleted_at', null)->count() <> 0) {
                $response = [
                    'status' => false,
                    'title' => 'Data sudah tersedia',
                    'text' => '',
                    'type' => 'error'
                ];

                return response()->json($response);
            }
            Type::where('id', $request->id)->update(
                [
                    'name' => $request->name,
                    'description' => $request->description
                ]
            );
            $response = [
                'status' => true,
                'title' => 'Berhasil menyunting data',
                'text' => '',
                'type' => 'success'
            ];
        } catch (\Exception $e) {
            throw $e;
            $response = [
                'status' => false,
                'title' => 'Gagal menyunting data',
                'text' => $e->getMessage(),
                'type' => 'error'
            ];
        }

        return response()->json($response);
    }

    public function deleteData(Request $request)
    {
        try {
            Type::where('id', $request->id)->delete();
            Material::where('type_id', $request->id)->update(
                [
                    'category_id' => 0
                ]
            );
            $response = [
                'status' => true,
                'title' => 'Berhasil menghapus data',
                'text' => '',
                'type' => 'success'
            ];
        } catch (\Exception $e) {
            throw $e;
            $response = [
                'status' => false,
                'title' => 'Gagal menghapus data',
                'text' => $e->getMessage(),
                'type' => 'error'
            ];
        }

        return response()->json($response);
    }
}
