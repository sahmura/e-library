<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Material;
use App\Transaction;
use App\User;

class AdminDashboardController extends Controller
{
    public function index()
    {
        $material = Material::where('deleted_at', null);
        $data['totalData'] = $material->count();
        $data['totalDownloads'] = $material->sum('downloads');
        $data['totalUser'] = User::where('role', 'student')->count();
        $lastPublish = $material->latest()->limit(5)->get();
        $populers = $material->orderBy('reads', 'DESC')->limit(5)->get();
        $data['totalPinjaman'] = Transaction::where('type', 'PINJAM')->count();
        return view('admin.dashboard.admindashboard', compact('data', 'lastPublish', 'populers'));
    }

    // dashboard
}
