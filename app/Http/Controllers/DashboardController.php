<?php

namespace App\Http\Controllers;

use App\Material;
use App\Prodi;
use Illuminate\Http\Request;
use App\Transaction;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class DashboardController extends Controller
{
    public function index()
    {
        $prodies = Prodi::orderBy('jenjang', 'ASC')->get();
        $materials = Transaction::where('user_id', Auth()->user()->id)->where('type', 'PINJAM')->with('material')->latest()->limit(3)->get();
        $materialsdipinjam = Transaction::where('user_id', Auth()->user()->id)->where('type', 'PINJAM')->whereDate('due_date', '>=', Carbon::now())->with('material')->latest()->get();
        $data['materialDipinjam'] = Transaction::where('user_id', Auth()->user()->id)->where('type', 'PINJAM')->count();
        $data['materialDiunduh'] = Transaction::where('user_id', Auth()->user()->id)->where('type', 'UNDUH')->count();
        return view('user.dashboard.userdashboard', compact('materials', 'materialsdipinjam', 'data', 'prodies'));
    }

    public function read($id)
    {
        $material = Transaction::where('user_id', Auth()->user()->id)
            ->where('type', 'PINJAM')
            ->whereDate('due_date', '>=', Carbon::now())
            ->where('material_id', $id)
            ->with('material.authors')->first();
        Material::where('id', $id)->increment('reads');
        $getExtension = explode('.', $material->material->download_path);
        if ($material == null) {
            return redirect('dashboard/my');
        }
        $extension = end($getExtension);
        return view('user.dashboard.userread', compact('material', 'extension'));
    }

    public function download($id)
    {
        try {
            $data = Material::where('id', $id)->first();
            // Transaction::create(
            //     [
            //         'material_id' => $id,
            //         'user_id' => Auth()->user()->id,
            //         'type' => 'UNDUH',
            //         'due_date' => ''
            //     ]
            // );
            Material::where('id', $id)->increment('downloads');
        } catch (\Exception $e) {
            throw $e;
        }

        return response()->download(('file/' . $data->download_path));
    }

    public function updatePrody(Request $request)
    {
        User::where('id', Auth()->user()->id)->update(
            [
                'prodi_id' => $request->prody_id
            ]
        );
        return redirect('dashboard');
    }

    public function userSetting()
    {
        $data = User::where('id', Auth()->user()->id)->first();
        $prodies = Prodi::orderBy('jenjang', 'ASC')->get();
        return view('user.dashboard.usersetting', compact('data', 'prodies'));
    }

    public function updateProfile(Request $request)
    {
        User::where('id', Auth()->user()->id)->update(
            [
                'name' => $request->name,
                'email' => $request->email,
                'telp' => $request->telp ?? '',
                'prodi_id' => $request->prody_id ?? 0,
                'alamat' => $request->alamat ?? ''
            ]
        );

        $response = [
            'status' => true,
            'title' => 'Berhasil Memperbaharui Data',
            'text' => '',
            'icon' => 'success'
        ];

        return response()->json($response);
    }

    public function updatePassword(Request $request)
    {
        try {
            $data = User::where('id', Auth()->user()->id)->first();
            if (Hash::check($request->oldpass, $data->password)) {
                User::where('id', Auth()->user()->id)->update(
                    [
                        'password' => Hash::make($request->password)
                    ]
                );
                $response = [
                    'status' => true,
                    'title' => 'Berhasil Memperbaharui Kata Sandi',
                    'text' => '',
                    'icon' => 'success'
                ];
            } else {
                $response = [
                    'status' => false,
                    'title' => 'Gagal Memperbaharui Kata Sandi',
                    'text' => 'Kata sandi lama tidak sesuai',
                    'icon' => 'error'
                ];
            }
        } catch (\Exception $e) {
            throw $e;
            $response = [
                'status' => false,
                'title' => 'Gagal Memperbaharui Kata Sandi',
                'text' => $e->getMessage(),
                'icon' => 'error'
            ];
        }

        return response()->json($response);
    }
}
