<?php

namespace App\Http\Controllers;

use App\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class PeminjamanController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $dataPinjam = Transaction::where('type', 'PINJAM')->with(['user.prodi', 'material'])->get();
            return DataTables::of($dataPinjam)
                ->addColumn(
                    'peminjam',
                    function ($dataPinjam) {
                        return $dataPinjam->user->name;
                    }
                )
                ->addColumn(
                    'material',
                    function ($dataPinjam) {
                        return $dataPinjam->material->title;
                    }
                )
                ->addColumn(
                    'tgl_pinjam',
                    function ($dataPinjam) {
                        return $dataPinjam->created_at;
                    }
                )
                ->addColumn(
                    'tgl_kembali',
                    function ($dataPinjam) {
                        return $dataPinjam->due_date;
                    }
                )
                ->addColumn(
                    'status',
                    function ($dataPinjam) {
                        return $dataPinjam->status;
                    }
                )
                ->addColumn(
                    'keterangan',
                    function ($dataPinjam) {
                        if ($dataPinjam->status == 'SUCCESS') {
                            $diff = Carbon::parse($dataPinjam->due_date)->diffForHumans();
                            return (Carbon::parse($dataPinjam->due_date)->format('Y-m-d') < Carbon::now()->format('Y-m-d')) ? 'Returned ' . $diff : 'Due ' . $diff;
                        } else {
                            return ucfirst(strtolower($dataPinjam->status)) . ' ' . Carbon::parse($dataPinjam->updated_at)->diffForHumans();
                        }
                    }
                )
                ->addColumn(
                    'action',
                    function ($dataPinjam) {
                        $button = '<div class="btn btn-group">
                            <button class="btn btn-info btn-sm btn-detail"
                                data-name="' . $dataPinjam->user->name . '"
                                data-email="' . $dataPinjam->user->email . '"
                                data-prodi="' . $dataPinjam->user->prodi->name . '"
                                data-material="' . $dataPinjam->material->title . '"
                                data-tglpinjam="' . $dataPinjam->created_at . '"
                                data-tglkembali ="' . $dataPinjam->due_date . '">
                                    <i class="mdi mdi-magnify"></i>
                            </button>';
                        if ($dataPinjam->status == 'SUCCESS') {
                            $button .= '<button class="btn btn-danger btn-sm btn-delete"
                                data-id="' . $dataPinjam->id . '">
                                    <i class="mdi mdi-delete"></i>
                            </button>';
                        }
                        $button .= '</div>';
                        return $button;
                    }
                )
                ->addIndexColumn()
                ->make(true);
        }
        $data['totalPinjamanHariIni'] = Transaction::where('type', 'PINJAM')->whereDate('created_at', Carbon::now())->count();
        $data['peminjamHariIni'] = Transaction::where('type', 'PINJAM')->whereDate('created_at', Carbon::now())->groupBy('user_id')->get('user_id');
        return view('admin.peminjaman.adminpeminjaman', compact('data'));
    }

    public function deletePeminjaman(Request $request)
    {
        try {
            Transaction::where('id', $request->id)->update(
                [
                    'status' => 'CANCEL'
                ]
            );
            $response = [
                'status' => true,
                'title' => 'Peminjaman berhasil dibatalkan',
                'text' => '',
                'type' => 'success'
            ];
        } catch (\Exception $e) {
            $response = [
                'status' => false,
                'title' => 'Peminjaman gagal dibatalkan',
                'text' => '',
                'type' => 'error'
            ];
        }

        return response()->json($response);
    }
}
