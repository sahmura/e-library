<?php

namespace App\Http\Controllers;

use App\Material;
use App\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MyMaterialController extends Controller
{
    public function index()
    {
        $materials = Transaction::where('user_id', Auth()->user()->id)
            ->where('type', 'PINJAM')
            ->whereDate('due_date', '>=', Carbon::now()->format('Y-m-d'))
            ->with('material')->latest()->get();
        $bookmarks = [];
        return view('user.my.mymaterial', compact('materials', 'bookmarks'));
    }
}
