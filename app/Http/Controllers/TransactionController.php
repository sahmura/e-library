<?php

namespace App\Http\Controllers;

use App\Material;
use App\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;

class TransactionController extends Controller
{
    public function borrow(Request $request)
    {
        try {
            DB::beginTransaction();
            Transaction::create(
                [
                    'user_id' => Auth()->user()->id,
                    'material_id' => $request->id,
                    'due_date' => $request->tanggal_kembali,
                    'created_at' => Carbon::parse($request->tanggal_pinjam)->format('Y-m-d, H:i:s'),
                    'type' => 'PINJAM',
                    'status' => 'SUCCESS'
                ]
            );
            DB::commit();
            $response = [
                'status' => true,
                'title' => 'Berhasil Meminjam Material',
                'text' => 'Material akan dikembalikan otomatis pada ' . Carbon::parse($request->tanggal_kembali)->locale('id')->isoFormat('dd MMMM YYYY'),
                'icon' => 'success'
            ];
        } catch (\Exception $e) {
            DB::rollback();
            $response = [
                'status' => false,
                'title' => 'Gagal Meminjam Material',
                'text' => $e->getMessage(),
                'icon' => 'error'
            ];
        }

        return response()->json($response);
    }

    public function download(Request $request)
    {
        try {
            DB::beginTransaction();
            Transaction::create(
                [
                    'user_id' => Auth()->user()->id,
                    'material_id' => $request->id,
                    'due_date' => $request->tanggal_kembali,
                    'created_at' => Carbon::parse($request->tanggal_pinjam)->format('Y-m-d, H:i:s'),
                    'type' => 'UNDUH',
                    'status' => 'SUCCESS'
                ]
            );
            DB::commit();
            $response = [
                'status' => true,
                'title' => 'Berhasil Mengunduh Material',
                'text' => '',
                'icon' => 'success'
            ];
        } catch (\Exception $e) {
            DB::rollback();
            $response = [
                'status' => false,
                'title' => 'Gagal Mengunduh Material',
                'text' => $e->getMessage(),
                'icon' => 'error'
            ];
        }
        return response()->json($response);
    }
}
