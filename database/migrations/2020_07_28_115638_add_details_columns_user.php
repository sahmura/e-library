<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDetailsColumnsUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('telp')->nullable();
            $table->text('alamat')->nullable();
            $table->bigInteger('prodi_id')->nullable();
            $table->bigInteger('faculty_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('telp');
            $table->dropColumn('alamat');
            $table->dropColumn('prodi_id');
            $table->dropColumn('faculty_id');
        });
    }
}
