<?php

use Illuminate\Database\Seeder;

class ListFacultySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('faculties')->delete();

        $data = [
            ['name' => 'Fakultas Ilmu Pendidikan'],
            ['name' => 'Fakultas Bahasa dan Seni'],
            ['name' => 'Fakultas Ilmu Sosial'],
            ['name' => 'Fakultas Matematika dan Ilmu Pengetahuan Alam'],
            ['name' => 'Fakultas Teknik'],
            ['name' => 'Fakultas Ilmu Keolahragaan'],
            ['name' => 'Fakultas Ekonomi'],
            ['name' => 'Fakultas Hukum'],
        ];

        DB::table('faculties')->insert($data);
    }
}
