## Install

1. Clone this project
2. Run `composer update`
3. Run `php artisan migrate`
4. Run `php artisan serve` to run local development

## Database

https://dbdiagram.io/d/5f191af71e6ca02dc1a4513c
