<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('search/{typeid?}/{categoryid?}/{tahun?}/{key?}', 'HomeController@search');
Route::get('detail/{id}', 'HomeController@detail');

Route::get('auth/google', 'LoginController@authGoogle');
Route::get('auth/google/redirect', 'LoginController@authGoogleRedirect');

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'statistik'], function () {
    Route::get('/', 'StatisticController@index');
    Route::post('/getTotalByCategory', 'StatisticController@getTotalByCategory');
    Route::post('/getTotalByType', 'StatisticController@getTotalByType');
    Route::post('/getTotalByYear', 'StatisticController@getTotalByYear');
    Route::post('/getTotalUserByProdiS', 'StatisticController@getTotalUserByProdiS');
    Route::post('/getTotalUserByProdiM', 'StatisticController@getTotalUserByProdiM');
    Route::post('/getTotalPinjamByYear', 'StatisticController@getTotalPinjamByYear');
    Route::post('/getTotalUnduhByYear', 'StatisticController@getTotalUnduhByYear');
});

Route::group(['prefix' => 'admin', 'middleware' => ['checkrole:admin,operator', 'auth']], function () {
    Route::get('', 'AdminDashboardController@index');
    Route::get('/statistik', 'StatisticController@indexAdmin');

    Route::group(['prefix' => 'katalog', 'middleware' => ['checkrole:admin,operator']], function () {
        Route::get('', 'KatalogController@index');
        Route::get('create', 'KatalogController@createPage');
        Route::post('addData', 'KatalogController@addData');
        Route::post('editData', 'KatalogController@editData');
        Route::post('deleteData', 'KatalogController@deleteData');
        Route::get('/{id}/edit', 'KatalogController@editPage');
        Route::get('/{id}/detail', 'KatalogController@detailPage');
    });

    Route::group(['prefix' => 'peminjaman', 'middleware' => ['checkrole:admin,operator']], function () {
        Route::get('', 'PeminjamanController@index');
        Route::post('/cancel', 'PeminjamanController@deletePeminjaman');
    });

    Route::group(['prefix' => 'user', 'middleware' => ['checkrole:admin']], function () {
        Route::get('', 'UserAdminController@index');
        Route::post('changeRole', 'UserAdminController@changeRole');
    });

    Route::group(['prefix' => 'author', 'middleware' => ['checkrole:admin,operator']], function () {
        Route::get('', 'AuthorController@index');
        Route::post('addData', 'AuthorController@addData');
        Route::post('editData', 'AuthorController@editData');
        Route::post('deleteData', 'AuthorController@deleteData');
        Route::post('getAuthor', 'AuthorController@getAuthor');
    });

    Route::group(['prefix' => 'category', 'middleware' => ['checkrole:admin,operator']], function () {
        Route::get('', 'CategoryController@index');
        Route::post('addData', 'CategoryController@addData');
        Route::post('editData', 'CategoryController@editData');
        Route::post('deleteData', 'CategoryController@deleteData');
    });

    Route::group(['prefix' => 'type', 'middleware' => ['checkrole:admin,operator']], function () {
        Route::get('', 'TypeController@index');
        Route::post('addData', 'TypeController@addData');
        Route::post('editData', 'TypeController@editData');
        Route::post('deleteData', 'TypeController@deleteData');
    });
});

Route::group(['prefix' => 'dashboard', 'middleware' => ['checkrole:admin,student', 'auth']], function () {
    Route::get('', 'DashboardController@index');
    Route::post('updatePrody', 'DashboardController@updatePrody');

    Route::group(['prefix' => 'explore'], function () {
        Route::get('/', 'ExploreController@index');
        Route::get('/{id}/detail', 'ExploreController@detail');
        Route::get('/search/{typeid?}/{categoryid?}/{tahun?}/{key?}', 'ExploreController@searchBy');
    });

    Route::get('/my', 'MyMaterialController@index');
    Route::get('/download/{id}', 'DashboardController@download');
    Route::get('/setting', 'DashboardController@userSetting');
    Route::post('/updateprofile', 'DashboardController@updateProfile');
    Route::post('/updatepassword', 'DashboardController@updatePassword');

    Route::group(['prefix' => 'transaction'], function () {
        Route::post('/pinjam', 'TransactionController@borrow');
        Route::post('/download', 'TransactionController@download');
        Route::post('/{id}/baca', 'TransactionController@read');
        Route::post('/{id}/unduh', 'TransactionController@download');
    });

    Route::group(['prefix' => 'read'], function () {
        Route::get('/{id}', 'DashboardController@read');
    });
});
